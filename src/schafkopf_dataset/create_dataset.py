from schafkopf.player import SmartPlayer, DumbPlayer
from schafkopf_gym.observation_player import ObservationPlayer
from schafkopf.schafkopf import GameState, Schafkopf
import random
from schafkopf_gym.gym_observation import onehot_observation, observation_space
from schafkopf.util import CannotPlayCardError
from schafkopf.gametype import GameType
from schafkopf.gameround import Turn


if __name__ == "__main__":
    players = [
        ObservationPlayer("playername"),
        ObservationPlayer("playername"),
        ObservationPlayer("playername"),
        ObservationPlayer("playername")
    ]

    # TODO replay information until first stich
    # have a look at the methods of observation players, and at the stages in schafkopf

    gametype = GameType.RAMSCH

    kommtraus = 0
    # for all stiche
    for i in range(8):

        turn = Turn(gametype, kommtraus)
        for p in players:
            observation = onehot_observation(player, turn)
            # TODO save in player specific dataset the tuple (observation, correct_cardindex)
            correct_card = 0
            p.set_card(correct_card)
            p.play_card(turn)
