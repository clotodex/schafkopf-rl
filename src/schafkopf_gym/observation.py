from schafkopf.gametype import GameType
from schafkopf.util import cardorder, colors
from schafkopf.gameround import Turn, Stich
from .observation_player import ObservationPlayer

from gym.spaces import flatdim, Box, Dict, Discrete, MultiDiscrete, MultiBinary, Tuple

NUM_PLAYERS=4

def observation_space_card():
    return Tuple((Discrete(len(cardorder)+1), Discrete(len(colors)+1)))
def observation_space_stich():
    #TODO a lot of redundant information (nonecards and - no longer - nonewinners) just leave zero
    return Tuple((
        Tuple((
            observation_space_card(),
            observation_space_card(),
            observation_space_card(),
            observation_space_card()
        )),
        Discrete(NUM_PLAYERS)
    ))

def observation_space_dict():
    space = Dict({
        #"first": Discrete(NUM_PLAYERS), # first should always be 0
        "gametype": Tuple((
            #   ober
            Discrete(2), # Discrete(1) not possible - one hot sampling
            #   unter
            Discrete(2),
            #   ruffarbe
            Discrete(4), # herz not possible
            #   trumpffarbe
            Discrete(5),
            #   multiplayergame / or number of co-players
            #   for now: solo or sauspiel
            Discrete(2),
            #   gameplayer / mitspieler / opponent
            # TODO Discrete(5),
        )),
        "gameplayer": Discrete(NUM_PLAYERS),
        #"knocked": Tuple((
        #    Discrete(1),
        #    Discrete(1),
        #    Discrete(1),
        #    Discrete(1)
        #)),
        #"kontra": Discrete(NUM_PLAYERS+1),
        #"retour": Discrete(NUM_PLAYERS+1),
        "stiche": Tuple(( # max 7 stiche already made
            observation_space_stich(),
            observation_space_stich(),
            observation_space_stich(),
            observation_space_stich(),
            observation_space_stich(),
            observation_space_stich(),
            observation_space_stich()
        )),
        "kommtraus": Discrete(NUM_PLAYERS),
        "cardsplayed": Tuple(( # max 3 cards on the table
            observation_space_card(),
            observation_space_card(),
            observation_space_card(),
        )),
        "me": Discrete(NUM_PLAYERS),
        "cards": Tuple((
            observation_space_card(),
            observation_space_card(),
            observation_space_card(),
            observation_space_card(),
            observation_space_card(),
            observation_space_card(),
            observation_space_card(),
            observation_space_card()
        )),
    })
    #print("flat dimensions: ", flatdim(space))
    #assert flatdim(space) == 625
    return space

def observation_space():
    space = observation_space_dict()
    flattened = flattenspace(space)
    return MultiDiscrete(flattened)

def flattenspace(space):
    if isinstance(space,Discrete):
        return [space.n]
    elif isinstance(space,Tuple):
        return sum((flattenspace(s) for s in space.spaces),[])
    elif isinstance(space,Dict):
        return sum((flattenspace(s) for s in space.spaces.values()),[])
    else:
        raise ValueError("Unexpected type: {}: {}".format(type(space), space))

def flattentuples(obs):
    if isinstance(obs,int):
        return [obs]
    elif isinstance(obs,tuple):
        return sum((flattentuples(o) for o in obs), [])
    elif isinstance(obs,dict):
        return sum((flattentuples(o) for o in obs.values()), [])
    else:
        raise ValueError("Unexpected type {}: {}".format(type(obs), obs))
    pass

def onehot_card(card):
    #print("card: ",card)
    face, color = card
    # +1 to have a NONE option => for logic maybe move to 0,0 spot
    face_index = cardorder.index(face) + 1 if face is not None else 0
    color_index = colors.index(color) + 1 if color is not None else 0
    return (face_index, color_index)

def onehot_observation(agent, turn):
    me = agent.table["me"]
    #TODO normalize in respect to 'me'

    if turn is None:
        turn = Turn(None, 0)

    # current cards in hand
    cards = tuple(
        onehot_card(agent.cards[i])
        if i < len(agent.cards)
        else onehot_card((None, None))
        for i in range(8)
    )

    # gametype
    gametype = agent.gametype.value-1

    gamename, gamecolor = agent.gametype.split_in_name_and_color()
    trumpfaces, trumpcolor = agent.gametype.split_in_faces_and_color()

    #TODO ober and unter could be inferred from trumpfaces
    if gamename == "SAUSPIEL":
        gametype = (
            1, #   ober
            1, #   unter
            [c for c in colors if c != "herz"].index(gamecolor) + 1, # ruffarbe
            colors.index("herz") + 1, # trumpffarbe = herz
            1, # multiplayergame / or number of co-players, for now: solo or sauspiel
            # TODO Discrete(5), # gameplayer / mitspieler / opponent
        ),
    elif gamename == "SOLO":
        gametype = (
            1, #   ober
            1, #   unter
            0, # ruffarbe
            colors.index(trumpcolor) + 1, # trumpffarbe
            0, # multiplayergame / or number of co-players, for now: solo or sauspiel
            # TODO Discrete(5), # gameplayer / mitspieler / opponent
        ),
    elif gamename == "WENZ":
        gametype = (
            0, #   ober
            1, #   unter
            0, # ruffarbe
            colors.index(trumpcolor) + 1 if trumpcolor is not None else 0, # trumpffarbe
            0, # multiplayergame / or number of co-players, for now: solo or sauspiel
            # TODO Discrete(5), # gameplayer / mitspieler / opponent
        ),
    elif gamename == "GEIER":
        gametype = (
            1, #   ober
            0, #   unter
            0, # ruffarbe
            colors.index(trumpcolor) + 1 if trumpcolor is not None else 0, # trumpffarbe
            0, # multiplayergame / or number of co-players, for now: solo or sauspiel
            # TODO Discrete(5), # gameplayer / mitspieler / opponent
        ),
    elif gamename == "RAMSCH":
        # should not be used for training
        # handle like solo
        gametype = (
            1, #   ober
            1, #   unter
            0, # ruffarbe
            colors.index(trumpcolor) + 1 if trumpcolor is not None else 0, # trumpffarbe
            0, # multiplayergame / or number of co-players, for now: solo or sauspiel
            # TODO Discrete(5), # gameplayer / mitspieler / opponent
        ),
    else:
        raise Exception("Unhandled gametype: {}".format(gamename))

    # turn
    #   cards played
    #   who started?
    cardsplayed = tuple(
        onehot_card(turn.stack[i])
        if i < len(turn.stack)
        else onehot_card((None, None))
        for i in range(3)
    )
    # normalize kommtraus
    kommtraus = (4 - me + turn.kommtraus) % 4

    #first = agent.table["first"]
    #assert first == 0
    gameplayer = (4 - me + agent.gameplayer) % 4 if agent.gameplayer is not None else 0

    knocked = tuple(
        1 if k else 0
        for k in agent.knocked
    )
    kontra = agent.kontra_player + 1 if agent.kontra_player is not None else 0
    retour = agent.retour_player + 1 if agent.retour_player is not None else 0

    # round
    #   dealer or first
    #   the gameplayer
    #   all last stichs with their winner
    # TODO normalize stich order -> so that card corresponds to who played it # or apply some sort of pooling
    stiche = tuple(
            (tuple(onehot_card(card) for card in agent.stiche[i].cards),
                (4 - me + agent.stiche[i].winner) % 4)
        if i < len(agent.stiche) else
            (tuple(onehot_card((None,None)) for i in range(4)), 0)
        for i in range(7) #every stich except last one is important to know everything about the game
    )
    observation = {
    #    "first": first,
        "gametype": gametype,
        "gameplayer": gameplayer,
        #"knocked": knocked,
        #"kontra": kontra,
        #"retour": retour,
        "stiche": stiche,
        "kommtraus": kommtraus,
        "cardsplayed": cardsplayed,
        "me": me,
        "cards": cards
    }
    observation = flattentuples(observation)
    # flattening will happen by gym
    return observation
