from schafkopf.player import AbstractPlayer, SmartPlayer

class ObservationPlayer(SmartPlayer):
    def set_next_card(self, index):
        self.next_card = index

    def reset(self):
        self.table = {}
        self.cards = []
        self.knocked = [False, False, False, False]
        self.gametype = None
        self.gameplayer = None
        self.stiche = []
        self.info = None
        self.rewards = None
        self.kontra_player = None
        self.retour_player = None

    def __init__(self, name):
        self.name = name
        self.reset()

    def info_table(self, first_ind, your_ind, names):
        self.table = {
                "first": first_ind,
                "me": your_ind,
                "names": names
        }

    def deal_first4(self, cards):
        self.cards += cards
        #TODO handle knocking
        return False

    def info_knocked(self, player):
        self.knocked[player] = True

    def deal_second4(self, cards):
        self.cards += cards

    #def wuerde(self, array_of_wuerdes):
    #    #TODO use code from SmartPlayer
    #    return SmartPlayer.wuerde(self, array_of_wuerdes)

    #def want_to_play(self, array_of_games_wuerdes_and_weiters, min_game):
    #    #TODO use code from SmartPlayer
    #    return SmartPlayer.want_to_play(self, array_of_games_wuerdes_and_weiters, min_game)

    def info_gameplayer(self, gameplayer, gametype):
        self.gameplayer = gameplayer
        self.gametype = gametype


    def kontra_chance(self, card):
        raise Exception("not implemented error")
    def retour_chance(self, card):
        raise Exception("not implemented error")
    def info_kontra(self, player):
        self.kontra_player = player
    def info_retour(self, player):
        self.retour_player = player


    def play_on_turn(self,turn):
        # TODO this needs to somehow be handled in the gym
        # e.g. have a prepared choice computed from the observations
        turn.place_card(self.cards, self.next_card)

    def info_stichresult(self,stich):
        self.stiche.append(stich)

    def rundenende(self,rewards,info):
        self.rewards = rewards
        self.info = info
