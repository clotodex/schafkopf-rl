from schafkopf.player import SmartPlayer, DumbPlayer
from .observation_player import ObservationPlayer
from schafkopf.schafkopf import GameState, Schafkopf
import random
import numpy as np
from .observation import onehot_observation, observation_space
from schafkopf.util import CannotPlayCardError
from schafkopf.gametype import GameType
from schafkopf.gameround import Turn

import sys
import gym
from gym import error, spaces, utils
from gym.utils import seeding

class SchafkopfEnv(gym.Env):
    metadata = {'render.modes': []}

    def __init__(self, playerclasses=[SmartPlayer, SmartPlayer, DumbPlayer], args=None, num_agents=1):
        assert(num_agents > 0)
        assert(num_agents < 5)
        self.num_agents = num_agents
        if args is None or not args:
            args = [("{} P{}".format("Smart" if classname == SmartPlayer else "Dumb", i+2)) for i,classname in enumerate(playerclasses)]
        self.playerclasses = playerclasses
        self.args = args
        self.action_space = spaces.Discrete(8) # 8 cards
        self.observation_space = observation_space()
        #TODO are you allowe to call this from init?
        #self.reset()

    def step(self, action):
        """

        Parameters
        ----------
        action :

        Returns
        -------
        ob, reward, episode_over, info : tuple
            ob (object) :
                an environment-specific object representing your observation of
                the environment.
            reward (float) :
                amount of reward achieved by the previous action. The scale
                varies between environments, but the goal is always to increase
                your total reward.
            episode_over (bool) :
                whether it's time to reset the environment again. Most (but not
                all) tasks are divided up into well-defined episodes, and done
                being True indicates the episode has terminated. (For example,
                perhaps the pole tipped too far, or you lost your last life.)
            info (dict) :
                 diagnostic information useful for debugging. It can sometimes
                 be useful for learning (for example, it might contain the raw
                 probabilities behind the environment's last state change).
                 However, official evaluations of your agent are not allowed to
                 use this for learning.
        """
        # cardindex = action
        # place card in current turn
        # if failed, then negative reward
        # return (observation, reward, done, debug)

        index = action
        self.agents[self.current_idx].set_next_card(index)

        reward, observation = self.advance_game()
        #print("reward: {}, observation: {}".format(reward, observation))
        valid_actions = self.get_valid_actions()
        return observation, reward, self.done, {'valid_actions': valid_actions}

    def get_valid_actions(self):
        if self.game.state != GameState.PLAY_ROUND:
            return [True for _ in range(8)]

        turn = self.game.stage.turn
        if turn is None:
            turn = Turn(self.game.context.gametype, self.game.stage.kommtraus)

        def check_card(i):
            try:
                self.game.context.gametype.check_rules(i, self.agents[self.current_idx].cards, turn)
                return True
            except CannotPlayCardError as e:
                return False

        return [check_card(i) for i in range(8)]


    def get_stich_reward(self):
        # get last stich made
        stich = self.game.context.stiche[-1]
        # get points
        points = stich.points()
        # check if stich is agents or mitspieler

        positive = False
        if stich.winner in self.gameplayers and self.current_agent_ind in self.gameplayers:
            positive = True
        elif stich.winner not in self.gameplayers and self.current_agent_ind not in self.gameplayers:
            positive = True

        reward = (1 if positive else -1) * points
        return reward

    def get_game_reward(self):
        stich = self.get_stich_reward()
        game = self.game.context.results[self.current_agent_ind]
        # TODO test other rewards (like goal oriented - when not playing your goal is only to not be schneidered)
        return stich + 10 * game

    def get_misplay_reward(self):
        def worst_game():
            # farbwenz with 8 laufende
            a = 10
            farbwenz = 5*a
            #schneider = a
            #schwarz = a
            laufende = 8 * a
            worstgame = - (farbwenz + laufende) # schneider + schwarz
            worstgame *= 3 # pay players
            worstgame *= (2*2*2*2) # knocking
            worstgame *= 2 # kontra
            worstgame *= 2 # retour
            worstgame *= 2 # tout
            assert(worstgame == -49920)
            return worstgame

        def worst_stich():
            worststich = -44
            return worststich

        # to not rather play schwarz than make a bad stich
        stich_multiplier = 8 - len(self.game.context.stiche)
        reward = stich_multiplier * worst_stich() + worst_game()
        return reward

    def dump_context(self):
        from pprint import pprint
        pprint(vars(self.game.context))
        with open(".contextdump", "a") as dump:
            pprint(vars(self.game.context), stream=dump)

    def advance_game(self):
        try:
            self.game.do_gamestep()
            while self.game.state is not GameState.PLAY_ROUND or self.game.stage.current_player not in self.agent_inds:
                self.game.do_gamestep()
                #print("step: ", self.game.state)
                if self.game.state == GameState.DONE:
                    #print("game done")
                    self.done = True
                    # TODO fix everybody getting a game result observation
                    return self.get_game_reward(), onehot_observation(self.agents[self.current_idx], None)#None
            self.current_agent_ind = self.game.stage.current_player
            self.current_idx = self.agent_inds.index(self.current_agent_ind)
        except CannotPlayCardError as e:
            # TODO according to https://ai.stackexchange.com/questions/2980/how-to-handle-invalid-moves-in-reinforcement-learning and https://ai.stackexchange.com/questions/3403/openai-baselines-dqn-handling-of-invalid-actions ignoring the invalid actions leads to better results
            #print("ERROR: ", e)
            self.done = True
            self.dump_context()
            if len(self.game.context.stiche) == 0:
                raise ValueError("first action failed")
            raise ValueError("game failed!")
            return self.get_misplay_reward(), onehot_observation(self.agents[self.current_idx], self.game.stage.turn)#None
        # TODO this is a nasty hack
        if self.game.context.gametype == GameType.RAMSCH:
            #print("not playing RAMSH")
            return 0, self.internal_reset()[0]
        #print("step: ", self.game.state)
        if len(self.game.context.stiche) > 0:
            reward = self.get_stich_reward()
        else:
            self.gameplayers = [self.agents[self.current_idx].gameplayer]
            gametypename, gamecolor = self.agents[self.current_idx].gametype.split_in_name_and_color()
            if gametypename == GameType.SAUSPIEL_EICHEL.split_in_name_and_color()[0]:
                self.gameplayers += [i for i,p in enumerate(self.game.context.players) if ("A", gamecolor) in p.cards]
            #print("gameplayers: ",self.gameplayers)
            reward = 0 # None not allowed
        return reward, onehot_observation(self.agents[self.current_idx], self.game.stage.turn)

    def seed(self, seed=None):
        if seed is not None:
            random.seed(seed)
            np.random.seed(seed)

    def reset(self):
        self._resets = 0
        return self.internal_reset()


    def internal_reset(self):
        # start new schafkopf game
        self._resets += 1
        self.done = False
        if not hasattr(self, "players"):
            self.agents = [ObservationPlayer("Agent #{}".format(i+1)) for i in range(self.num_agents)]
            self.players = [
                    self.agents[0], # TODO
                    self.playerclasses[0](*self.args[0]),
                    self.playerclasses[1](*self.args[1]),
                    self.playerclasses[2](*self.args[2]),
            ]
        else:
            for p in self.players:
                p.reset()
        #indices = range(4)
        #tmp = list(zip(indices, players))
        random.shuffle(self.players)
        #indices, self.players = zip(*tmp)

        self.game = Schafkopf(self.players, random.randrange(4))
        self.agent_inds = [self.game.context.players.index(a) for a in self.agents]
        self.current_agent_ind = None

        reward, observation = self.advance_game()
        #print("reward: {}, observation: {}".format(reward, observation))
        valid_actions = self.get_valid_actions()
        return observation, {'valid_actions': valid_actions}

    def render(self, mode='human'):
        outfile = StringIO() if mode == 'ansi' else sys.stdout

        for player in self.players:
            outfile.write("{}: {}\n".format(player.name, player.cards))
        if hasattr(self.game.stage, 'turn') and self.game.stage.turn:
            outfile.write("Turn: {}\n".format(self.game.stage.turn.stack))
        else:
            outfile.write("\n")
        if self.game.context.stiche:
            #TODO could also print all stiche
            outfile.write("Last Stich: {}\n".format(self.game.context.stiche[-1].cards))
        else:
            outfile.write("\n")
        outfile.write("\n")

        if mode != 'human':
            with closing(outfile):
                return outfile.getvalue()
    def close(self):
        # remove the schafkopf game
        pass
