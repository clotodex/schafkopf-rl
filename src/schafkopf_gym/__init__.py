from .environment import SchafkopfEnv
from schafkopf.player import SmartPlayer, DumbPlayer

from gym.envs.registration import register

register(
    id='Schafkopf-v0',
    entry_point='schafkopf_gym:SchafkopfEnv',
    kwargs={
        'playerclasses': [SmartPlayer, SmartPlayer, DumbPlayer],
        'args': [("Smart P2",), ("Smart P3",), ("Dumb P4",)],
    },
)
