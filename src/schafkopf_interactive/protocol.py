import json
import struct


def _get_block(s, count):
    if count <= 0:
        return b''
    buf = b''
    while len(buf) < count:
        buf2 = s.recv(count - len(buf))
        if not buf2:
            # error or just end of connection?
            if buf:
                raise RuntimeError("underflow")
            else:
                return b''
        buf += buf2
    return buf

def _send_block(s, data):
    while data:
        data = data[s.send(data):]

class Logger(object):
    def __init__(self, debug=True):
        self.dodebug = debug
        pass
    def debug(self, msg):
        if self.dodebug:
            print("[DEBUG]",msg)

l = Logger(debug=False)

import jsonpickle
import enum

class MessageType(enum.Enum):
    METHOD = 1
    DONE = 2

class SchafkopfJsonProtocol(object):
    def __init__(self, socket):
        self.socket = socket

    def send_data(self, data):
        l.debug("preparing data")
        #s = json.dumps(data, cls=EnumEncoder)
        s = jsonpickle.encode(data)
        data = s.encode('utf8')
        blen = len(data)

        header = struct.pack('>I', blen)
        l.debug("sending header")
        _send_block(self.socket, header)
        l.debug("sending data")
        _send_block(self.socket, data)
        l.debug("sent")

    def send_method(self, methodname, args = None):
        l.debug("sending method")
        payload = {}
        payload['method'] = methodname
        if args:
            payload['args'] = args
        self.send_data(payload)
        l.debug("sent")

    def receive(self):
        l.debug("receiving count")
        count = struct.unpack('>I', _get_block(self.socket, 4))[0]
        l.debug("receiving block")
        msg = _get_block(self.socket, count)
        l.debug("decoding bytes")
        s = msg.decode('utf8')
        l.debug("decoding json")
        #payload = json.loads(s)
        payload = jsonpickle.decode(s)
        l.debug("received")
        return payload

    def receive_and_call(self, objtocall):
        l.debug("receiving method")
        payload = self.receive()
        l.debug("getting method")
        method = getattr(objtocall, payload['method'])
        l.debug("calling method")
        if 'args' in payload:
            return method(*payload['args'])
        else:
            return method()
        l.debug("done")

    # alternative if data length is very large
    # https://stackoverflow.com/questions/27428936/python-size-of-message-to-send-via-socket
    #def _get_count(s):
    #    buf = ''
    #    while True:
    #        c = s.recv(1)
    #        if not c:
    #            # error or just end of connection/
    #            if buf:
    #                raise RuntimeError("underflow")
    #            else:
    #                return -1
    #        if c == '|':
    #            return int(buf)
    #        else:
    #            buf += c

    #def get_msg(s):
    #    return _get_block(s, _get_count(s))

    #def send_msg(s, data):
    #    _send_block(s, str(len(data)) + '|')
    #    _send_block(s, data)
