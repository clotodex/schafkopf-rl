from schafkopf import Schafkopf
from schafkopf.schafkopf import GameState
from schafkopf.gametype import GameType
from schafkopf.player import SmartPlayer
from protocol import SchafkopfJsonProtocol as Protocol
from serverplayer import ServerPlayer
from schafkopf_agent.ai_player import AIPlayer

import argparse
import socket
import threading
import random

class SchafkopfServer(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))

    def wait_for_players(self, playercount):
        self.sock.listen(5)
        print("waiting for players...")
        protocols = []
        for i in range(playercount):
            client, address = self.sock.accept()
            print("found player")
            protocols.append(Protocol(client))
            #client.settimeout(60)
            #threading.Thread(target = self.listenToClient,args = (client,address)).start()
        return protocols

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--opponents', default="smart")
    parser.add_argument('-c', '--checkpointfile', default="./models/checkpoints/model_v0.0.0_PPO2_ep10000_runstrategy_128_128-128_128-128/schafkopf_chkpt_99.zip")
    args = parser.parse_args()
    port_num = 8787
    playeramount = 1
    ai_checkpointfile = args.checkpointfile

    server = SchafkopfServer('',port_num)
    protocols = server.wait_for_players(playeramount)
    players = [
            ServerPlayer(p)
            for p in protocols
    ]

    for i in range(4-playeramount):
        if args.opponents == "smart":
            players.append(SmartPlayer("Smart P{}".format(i)))
        else:
            players.append(AIPlayer("Agent P{}".format(i), ai_checkpointfile))

    random.shuffle(players)
    dealer_ind = random.randrange(4)

    while True:
        print("Create Schafkopf")
        schafkopf = Schafkopf(players, dealer_ind)
        while schafkopf.state != GameState.DONE:
            # TODO check if all players still here or anyone quit
            if schafkopf.state == GameState.PLAY_ROUND:
                if schafkopf.context.gametype == GameType.RAMSCH:
                    print("WILL NOT PLAY RAMSCH!!!")
                    break
            schafkopf.do_gamestep()
        dealer_ind = (dealer_ind + 1)%4

    print("Closing sockets")
    for pr in protocols:
        pr.socket.close()
