import socket
from clientplayer import ClientPlayer
from protocol import SchafkopfJsonProtocol

import curses
from curses import wrapper
import time
import colorutil
from colorutil import Color, colorfmt, addcolorstr, cleanstr

def main(stdscr):
    #stdscr.nodelay(True)

    if not curses.has_colors():
        endwin()
        print("Your terminal does not support color")
        exit(1)

    colorutil.initialize()

    stdscr.clear()
    curses.noecho()
    curses.curs_set(0)

    num_rows, num_cols = stdscr.getmaxyx()
    center_row = num_rows // 2
    center_col = num_cols // 2

    def center_text(row, text):
        stdscr.move(row, center_col - (len(cleanstr(text)) // 2))
        addcolorstr(stdscr, text)
        #stdscr.addstr(text)

    # y, x, string
    r = center_row-6
    #center_text(r, "Welcome to %RED%Schafkopf%D%!")
    center_text(r, "Welcome to " + colorfmt('Schafkopf', Color.RED) + "!")
    r += 1
    center_text(r,"=======================")
    r += 2

    curses.echo()
    curses.curs_set(1)
    center_text(r,"Please enter your name: ")
    r += 2
    stdscr.refresh()
    name = stdscr.getstr().decode('utf8')
    curses.noecho()
    curses.curs_set(0)
    #TODO validate name


    HOST = '127.0.0.1'  # The server's hostname or IP address
    PORT = 8787        # The port used by the server

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        connected = False
        points = 0
        while not connected:
            try:
                center_text(r,"Connecting to server ")
                stdscr.clrtoeol()
                stdscr.addstr('.'*(points+1))
                stdscr.refresh()
                s.connect((HOST, PORT))
                r += 1
                connected = True
                center_text(r,"Connected!")
                stdscr.refresh()
            except ConnectionRefusedError as err:
                center_text(r+1,"Connection Refused!")
                stdscr.refresh()
                points = (points + 1) % 4
                time.sleep(1)

        client_player = ClientPlayer(stdscr, name)

        protocol = SchafkopfJsonProtocol(s)
        while True:
            ret = protocol.receive_and_call(client_player)
            if ret is not None:
                protocol.send_data(ret)

if __name__ == '__main__':
    wrapper(main)
