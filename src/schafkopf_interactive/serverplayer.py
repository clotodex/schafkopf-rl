from schafkopf.player import AbstractPlayer
from protocol import SchafkopfJsonProtocol

class ServerPlayer(AbstractPlayer):
    def reset(self):
        self.cards = []
        self.protocol.send_method("reset")

    def __init__(self, protocol):
        self.protocol = protocol
        self.ask_for_name()

    def ask_for_name(self):
        print("asking for name")
        self.protocol.send_method("ask_for_name")
        payload = self.protocol.receive()
        print("got name '{}'".format(payload))
        self.name = payload

    def info_table(self, first_ind, your_ind, names): #maybe even player info like name, points etc
        print("info table")
        self.protocol.send_method("info_table", [first_ind, your_ind, names])

    def deal_first4(self, cards):
        print("dealing first 4")
        self.cards += cards
        self.protocol.send_method("deal_first4", [cards])
        payload = self.protocol.receive()
        print("got knocked", payload)
        return payload

    def info_knocked(self, player):
        print("info knocked")
        self.protocol.send_method("info_knocked", [player])

    def deal_second4(self, cards):
        print("dealing second 4")
        self.cards += cards
        self.protocol.send_method("deal_second4", [cards])

    def wuerde(self, array_of_wuerdes, min_game):
        print("wuerde")
        self.protocol.send_method("wuerde", [array_of_wuerdes, min_game])
        payload = self.protocol.receive()
        print("got wuerde",payload)
        return payload

    def want_to_play(self, array_of_games_wuerdes_and_weiters, min_game):
        self.protocol.send_method("want_to_play", [array_of_games_wuerdes_and_weiters, min_game])
        return self.protocol.receive()

    def info_gameplayer(self, gameplayer, gametype):
        self.protocol.send_method("info_gameplayer", [gameplayer, gametype])

    def kontra_chance(self, card):
        # TODO async, da nur solange zweite karte nicht gelegt ist
        raise Exception("not overridden error")

    def retour_chance(self, card):
        # TODO async, da nur solange zweite karte nicht gelegt ist
        raise Exception("not overridden error")

    def info_kontra(self, player):
        self.protocol.send_method("info_kontra", [player])

    def info_retour(self, player):
        self.protocol.send_method("info_retour", [player])

    def play_on_turn(self,turn):
        self.protocol.send_method("play_on_turn", [turn])
        cardind = self.protocol.receive()
        turn.place_card(self.cards, cardind)

    def info_stichresult(self,stich):
        self.protocol.send_method("info_stichresult", [stich])

    def rundenende(self,info):
        self.protocol.send_method("rundenende", [info])
