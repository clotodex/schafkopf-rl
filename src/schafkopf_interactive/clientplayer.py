from schafkopf.util import CannotPlayCardError, cardindex
from schafkopf.gametype import GameType
from collections import Counter

from schafkopf.player import AbstractPlayer
from protocol import SchafkopfJsonProtocol

import curses
import time
from colorutil import Color, colorfmt, addcolorstr, cleanstr

#   ___   ___   ___   ___   ___   ___   ___   ___
#  |A  | |K  | |Q  | |J  | |10 | |9  | |8  | |7  |
#  | e | | e | | e | | e | | e | | e | | e | | e |  yellow
#  |___| |___| |___| |___| |___| |___| |___| |___|
#   ___   ___   ___   ___   ___   ___   ___   ___
#  |A  | |K  | |Q  | |J  | |10 | |9  | |8  | |7  |
#  | g | | g | | g | | g | | g | | g | | g | | g |  green
#  |___| |___| |___| |___| |___| |___| |___| |___|
#   ___   ___   ___   ___   ___   ___   ___   ___
#  |A  | |K  | |Q  | |J  | |10 | |9  | |8  | |7  |
#  | ♥ | | ♥ | | ♥ | | ♥ | | ♥ | | ♥ | | ♥ | | ♥ |  red
#  |___| |___| |___| |___| |___| |___| |___| |___|
#   ___   ___   ___   ___   ___   ___   ___   ___
#  |A  | |K  | |Q  | |J  | |10 | |9  | |8  | |7  |
#  | @ | | @ | | @ | | @ | | @ | | @ | | @ | | @ |  orange/braun
#  |___| |___| |___| |___| |___| |___| |___| |___|
#


def visualize_card(card):
    colormap = {
        # needs to be lazy loaded
        "eichel": ('e', Color.YELLOW),
        "gras": ('g', Color.GREEN),
        "herz": ('♥', Color.RED),
        "schellen": ('@', Color.CYAN)
    }

    face,color = card
    return [' ___ ',
            '|{} |'.format(colorfmt("{}{}".format(face,'' if len(face) > 1 else ' '), colormap[color][1])),
            '| {} |'.format(colorfmt(*colormap[color])),
            '|___|']

def addstr(win,row,col,text):
    win.move(row,col)
    addcolorstr(win,text)

def curses_input(win,row,col,message):
    curses.echo()
    curses.curs_set(1)
    addstr(win,row,col,message)
    win.clrtoeol()
    win.refresh()
    user_input = win.getstr().decode('utf8')
    curses.noecho()
    curses.curs_set(0)
    return user_input

def center_text(win, row, text):
    center_col = win.getmaxyx()[1] // 2
    addstr(win,row, center_col - (len(cleanstr(text)) // 2), text)

class ClientPlayer(AbstractPlayer):

    # TODO use char input or getmouse with clickposition for card selection
    # maybe work with mousemasks

    def card_interface(self):
        cardlen = 5
        spacing = 2
        maxrow, maxcol = self.win_bottom.getmaxyx()
        col = maxcol//2 - (len(self.cards)*(cardlen+spacing)-spacing) //2
        row = 1 #maxrow - 8
        center_text(self.win_bottom, row, "Your cards:")
        row += 1
        for cardnum, card in enumerate(self.sorted_cards, start=1):
            vis = visualize_card(card)
            for i,line in enumerate(vis):
                addstr(self.win_bottom,row+i,col,line)
            addstr(self.win_bottom,row + len(vis) + 1, col + (cardlen//2), str(cardnum))
            col += cardlen + spacing
        self.win_bottom.refresh()

    def message(self, message):
        message = str(message)
        m = self.win_msg.getmaxyx()[1]-2
        if len(message) > m:
            self.message(message[m:])
            message = message[0:m]
        self.win_msg.insertln()
        addstr(self.win_msg,1,0,message)
        self.win_msg.refresh()

    def __init__(self, stdscr, name):
        self.name = name
        self.stdscr = stdscr

        maxrows, maxcols = stdscr.getmaxyx()

        # Idea:
        # s = stich, m = messagesm, l = last stich
        # 0------------
        # | m | p2| l |
        # | p1| s | p3|
        # |           |
        # |   player  |
        # |------------

        width = maxcols // 3
        middle_width = maxcols - 2*width

        height = int(maxrows / 3.5)
        bottomheight = maxrows - 2*height

        # TODO deal with odd width
        self.win_left   = curses.newwin(height,width,        height,  0)
        self.win_top    = curses.newwin(height,middle_width, 0,       width)
        self.win_right  = curses.newwin(height,width,        height,  2*width)
        self.win_bottom = curses.newwin(bottomheight,maxcols,2*height,0)
        self.win_middle = curses.newwin(height,middle_width, height,  width)
        self.win_msg    = curses.newwin(height,width,        0,       0)
        self.win_stich  = curses.newwin(height,width,        0,       2*width)

        self.win_middle.border()

        self.win_left.refresh()
        self.win_top.refresh()
        self.win_right.refresh()
        self.win_bottom.refresh()
        self.win_middle.refresh()
        self.win_msg.refresh()
        self.win_stich.refresh()
        # dont call reset - will be called by protocol

    def reset(self):
        self.cards = []
        self.sorted_cards = []
        self.game_choice = None

    def ask_for_name(self):
        return self.name

    def info_table(self, first_ind, your_ind, names): #maybe even player info like name, points etc
        self.first_ind = first_ind
        self.me = your_ind
        self.playernames = names

        windows = [self.win_left, self.win_top, self.win_right]
        for i in range(3):
            pind = (self.me + 1 + i) % 4
            name = self.playernames[pind]
            maxrow, maxcol = windows[i].getmaxyx()
            center_col = maxcol // 2
            center_row = maxrow // 2
            windows[i].clear()
            addstr(windows[i],center_row,center_col - len(name)//2,name)
            windows[i].refresh()

    def deal_first4(self, cards):
        # TODO can return a knock
        self.cards = cards
        self.card_interface()
        return False

    def info_knocked(self, player):
        raise Exception("not overridden error")

    def deal_second4(self, cards):
        self.cards += cards
        self.sorted_cards = sorted(self.cards, key=cardindex, reverse=True)
        self.card_interface()

    # eine würde runde
    def wuerde(self, array_of_wuerdes, min_game):
        windows = [self.win_left, self.win_top, self.win_right]
        wuerde_already = False
        for i in range(self.me, 0, -1):
            wuerde_message = 'Weiter!'
            if array_of_wuerdes[-i] and not wuerde_already:
                wuerde_message = 'I dad!'
                wuerde_already = True
            elif array_of_wuerdes[-i] and wuerde_already:
                wuerde_message = 'I dad a!'
            maxrow, maxcol = windows[-i].getmaxyx()
            center_col = maxcol // 2
            center_row = maxrow // 2
            addstr(windows[-i],center_row + 2, center_col - len(wuerde_message)//2, wuerde_message)
            windows[-i].refresh()
            time.sleep(0.5)
        self.win_bottom.clear()
        self.card_interface()
        y,x = self.win_bottom.getyx()
        r = y+2# - 20
        s = "Select weiter or game type you want to play: "
        col = self.win_bottom.getmaxyx()[1] // 2 - (len(s) // 2)
        addstr(self.win_bottom,r,col,"Which game do you want to play?")
        r += 1
        addstr(self.win_bottom,r,col,"Others: {}".format(array_of_wuerdes))
        r += 2
        choices = {
            '1': 'weiter',
            '21': 'Sauspiel Eichel', '22': 'Sauspiel Gras', '23': 'Sauspiel Schellen',
            '31': 'Solo Eichel', '32': 'Solo Gras', '33': 'Solo Schellen', '34': 'Solo Herz',
            '4': 'Wenz',
            '5': 'Geier'
        }
        for key, choice in choices.items():
            addstr(self.win_bottom,r,col,'{}: {}'.format(key, choice))
            r += 1
        r += 1
        user_choice = ''
        while True:
            user_choice = curses_input(self.win_bottom, r, col, "Select weiter or game type you want to play: ")
            if str(user_choice) in choices.keys():
                game_choice = choices[user_choice]
                if game_choice == 'weiter':
                    self.game_choice = None
                    break
                game_choice_as_game_type = game_choice.upper().replace(' ', '_')
                g = GameType[game_choice_as_game_type]
                if g < min_game:
                    addstr(self.win_bottom,r,col,"The selected game '{}' is not higher than minimum game '{}'".format(user_choice, min_game.name))
                    self.win_bottom.clrtoeol()
                    self.win_bottom.refresh()
                    continue
                self.game_choice = g
                break
            addstr(self.win_bottom,r,col,"Please select a valid choice! (not {})".format(user_choice))
            self.win_bottom.clrtoeol()
            self.win_bottom.refresh()

        self.win_bottom.clear()
        if self.game_choice is None:
            return False
        return True

    # n offene runde oder weiter bis das höchste spiel gefunden wurde
    def want_to_play(self, array_of_games_wuerdes_and_weiters, min_game):
        self.message("Array: " + str(array_of_games_wuerdes_and_weiters))
        self.message("Min_game: " + str(min_game))
        self.message("g: {}".format(self.game_choice))
        return self.game_choice

    def info_gameplayer(self, gameplayer, gametype):
        self.gametype = gametype
        self.gameplayer = gameplayer
        if gametype == GameType.RAMSCH:
            self.message("RAMSCH!")
            return
        self.message("Player '{}' plays a {}".format(self.playernames[gameplayer], gametype.name))
        windows = [self.win_left, self.win_top, self.win_right, self.win_bottom]
        maxrow, maxcol = windows[-(self.me-gameplayer+1)].getmaxyx()
        center_col = maxcol // 2
        center_row = maxrow // 2
        windows[-(self.me-gameplayer+1)].addstr(center_row + 2, center_col - len(gametype.name)//2, gametype.name)
        windows[-(self.me-gameplayer+1)].refresh()

    # TODO bis inklusiv die erste karte liegt kann man kontra
    # dann kann man retour -> dann geht das spiel zurück an die spieler
    # kann sich NICHT wiederholen (a la sauspiel.de)
    # (auch genannt stoß)
    def kontra_chance(self, card):
        # TODO async, da nur solange zweite karte nicht gelegt ist
        raise Exception("not overridden error")

    def retour_chance(self, card):
        # TODO async, da nur solange zweite karte nicht gelegt ist
        raise Exception("not overridden error")

    def info_kontra(self, player):
        raise Exception("not overridden error")

    def info_retour(self, player):
        raise Exception("not overridden error")

    def draw_turn(self, turn):
        self.win_middle.clear()
        self.win_middle.border()
        maxrow, maxcol = self.win_middle.getmaxyx()
        center_row = maxrow // 2
        center_col = maxcol // 2
        s = "'{}' started the turn:".format(self.playernames[turn.kommtraus])
        addstr(self.win_middle,1,center_col-len(s)//2, s)

        cardlen = 5
        cardheight = 4
        vertical_offset = 3
        horizontal_offset = 10
        center_offsets = {
            0: (vertical_offset ,0),
            1: (0,-horizontal_offset),
            2: (-vertical_offset,0),
            3: (0,horizontal_offset),
        }
        for i,card in enumerate(turn.stack):
            relative_ind = (4 - self.me + turn.kommtraus + i) % 4
            rowoff, coloff = center_offsets[relative_ind]
            vis = visualize_card(card)

            for j, line in enumerate(vis):
                addstr(self.win_middle,center_row + rowoff + j - cardheight//2,
                                       center_col + coloff - cardlen//2,
                                       line)
            self.win_middle.refresh()
            time.sleep(0.5)
        self.win_middle.refresh()

    def info_turn(self,turn):
        # TODO should be called on every change to the turn so that we can redraw on multiplayer
        pass

    def play_on_turn(self,turn):
        self.win_bottom.clear()
        self.draw_turn(turn)
        self.card_interface()
        y,x = self.win_bottom.getyx()
        r = y+2# - 20
        s = "Select card to play: "
        col = self.win_bottom.getmaxyx()[1] // 2 - (len(s) // 2)
        while True:
            try:
                user_choice = curses_input(self.win_bottom, r, col, s)
                user_choice = int(user_choice)
                if user_choice < 1 or user_choice > len(self.cards):
                    addstr(self.win_bottom,r+1,col,"Please enter a card in range ({}-{})".format(1,len(self.cards)))
                    self.win_bottom.clrtoeol()
                    self.win_bottom.refresh()
                    continue

                i = self.cards.index(self.sorted_cards[user_choice - 1])
                self.gametype.check_rules(i, self.cards, turn)
                del self.cards[i]
                del self.sorted_cards[user_choice - 1]
                self.win_bottom.clear()
                self.card_interface()
                return i
            except ValueError as verr:
                if not verr.message.startswith('invalid literal for int()'):
                    raise verr
                addstr(self.win_bottom,r+1,col,"%RED%Please enter a number! {}%D%".format(verr))
                self.win_bottom.clrtoeol()
                self.win_bottom.refresh()
            except CannotPlayCardError as e:
                addstr(self.win_bottom,r+1,col,"%RED%This card is not allowed! {}%D%".format(e))
                self.win_bottom.clrtoeol()
                self.win_bottom.refresh()

    def info_stichresult(self,stich):
        self.win_stich.clear()
        maxrow, maxcol = self.win_stich.getmaxyx()
        cardheight = 4
        r = maxrow // 2 - cardheight//2 - 1
        center_text(self.win_stich, r, "Last Stich:")
        r+=1
        #center_text(self.win_stich, r, "started by {}".format(self.playernames[stich.kommtraus]))
        cardlen = 5
        spacing = 2
        col = maxcol//2 - (len(stich.cards)*(cardlen+spacing)-spacing) //2
        for card in stich.cards:
            vis = visualize_card(card)
            for i,line in enumerate(vis):
                addstr(self.win_stich,r+i,col,line)
            col += cardlen + spacing
        r += cardheight + 1
        center_text(self.win_stich, r, "made by {}".format(self.playernames[stich.winner]))
        self.win_stich.refresh()

    def rundenende(self,info):
        self.win_middle.clear()
        center_text(self.win_middle, 1, "Round Over!")
        maxrow, maxcol = self.win_middle.getmaxyx()
        center_row = maxrow //2
        center_col = maxcol //2

        vertical_offset = 2
        horizontal_offset = 5
        for i,money in enumerate(info):
            relative_ind = (4 - self.me + i) % 4

            mtext = "%RED%{}%D%".format(money) if money < 0 else "%GREEN%{}%D%".format(money)
            if relative_ind == 0:
                center_text(self.win_middle, center_row + vertical_offset, mtext)
            if relative_ind == 2:
                center_text(self.win_middle, center_row - vertical_offset, mtext)
            if relative_ind == 1:
                addstr(self.win_middle, center_row, center_col - horizontal_offset - len(str(money)), mtext)
            if relative_ind == 3:
                addstr(self.win_middle, center_row, center_col + horizontal_offset, mtext)

        self.win_middle.refresh()
        # TODO print laufende and punkte

        # TODO remove cards on table
        self.message("Round finished {}".format(info))

    #TODO abrechnung
