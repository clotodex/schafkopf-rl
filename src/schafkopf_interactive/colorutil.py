import curses
import re

class Color(object):
    pass

def initialize():
    curses.start_color()
    curses.use_default_colors()

    # TODO init_color can override colors => make colors more bright
    colors = {
        "BLACK"  : curses.COLOR_BLACK  ,
        "RED"    : curses.COLOR_RED    ,
        "GREEN"  : curses.COLOR_GREEN  ,
        "YELLOW" : curses.COLOR_YELLOW ,
        "BLUE"   : curses.COLOR_BLUE   ,
        "MAGENTA": curses.COLOR_MAGENTA,
        "CYAN"   : curses.COLOR_CYAN   ,
        "WHITE"  : curses.COLOR_WHITE  ,
    }

    setattr(Color, "D", 0)
    setattr(Color, "DEFAULT", 0)
    for i,(name,val) in enumerate(colors.items()):
        setattr(Color, name, i)
        curses.init_pair(i, val, -1) # -1 is default background
        #TODO add all possible forground / background combinations Color.BLACK_ON_WHITE etc

def colorfmt(text, color):
    all_attributes = dir(Color)
    candidates = [attr for attr in dir(Color) if getattr(Color, attr) == color]
    if not candidates:
        raise ValueError("Color {} not found".format(color))
    return "%{}%{}%D%".format(candidates[0], text)

def addcolorstr(win, text, forcecolor=None):
    #m = re.search('%(.+?)%', text)
    #if m:
    #    found = m.group(1)
    if len(text) == 0:
        return

    start = text.find('%')
    if start < 0:
        # uncolored
        if forcecolor:
            win.addstr(text, curses.color_pair(forcecolor))
        else:
            win.addstr(text)
        return

    end = text.find('%', start+1)
    if end < 0:
        raise ValueError("Unfinished Color sequence!")
    if end-start == 1:
        #actual % sign
        if forcecolor:
            win.addstr(text[:end], curses.color_pair(forcecolor))
        else:
            win.addstr(text[:end])
        addcolorstr(win, text[end+1:])
        return
    col = text[start+1:end]
    if not hasattr(Color,col):
        raise ValueError("Unknown Color {}".format(col))

    color = getattr(Color, col)
    if forcecolor:
        win.addstr(text[:start], curses.color_pair(forcecolor))
    else:
        win.addstr(text[:start])
    # maybe replace with direct text print
    addcolorstr(win, text[end+1:], forcecolor=color)

def cleanstr(text):
    if len(text) == 0:
        return ''

    start = text.find('%')
    if start < 0:
        return text

    end = text.find('%', start+1)
    if end < 0:
        raise ValueError("Unfinished Color sequence!")
    if end-start == 1:
        #actual % sign
        return text[:end] + cleanstr(text[end+1:])
        win.addstr(text[:start])
    # maybe replace with direct text print
    return text[:start] + cleanstr(text[end+1:])
