from .gametype import GameType
from . import util

class Stich():
    def __init__(self, stack, winner, kommtraus):
        self.cards = stack
        self.winner = winner
        self.kommtraus = kommtraus

    def points(self):
        return sum(util.faces[f] for (f,_) in self.cards)

class Turn():
    def __init__(self, gametype, kommtraus):
        # FIXME
        self.stack = []
        self.gametype = gametype
        self.kommtraus = kommtraus

    def place_card(self, cards, ind):
        #TODO check gamerules with gametype
        self.gametype.check_rules(ind, cards, self)
        card = cards[ind]
        self.stack.append(card)
        del cards[ind]

    def determine_winner(self):
        highest_card = self.stack[0]
        highest_ind = 0
        for ind in range(1,4):
            card = self.stack[ind]
            if self.gametype.compare_cards(highest_card, card):
                highest_card = card
                highest_ind = ind
        winner = highest_ind
        return winner
