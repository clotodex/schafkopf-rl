from .. import util
from ..util import CannotPlayCardError
from ..gametype import GameType
from collections import Counter
from .abstract import AbstractPlayer

class SmartPlayer(AbstractPlayer):
    def reset(self):
        self.cards=[]
        self.first=None
        self.ind=None
        self.gameplayer=None
        self.gametype=None
        self.playernames=None

    def __init__(self, name):
        self.name = name
        self.reset()

    def info_table(self, first_ind, your_ind, names): #maybe even player info like name, points etc
        self.first = first_ind
        self.ind = your_ind
        self.playernames = names

    def deal_first4(self, cards):
        self.cards += cards
        return False

    def info_knocked(self, player):
        pass

    def deal_second4(self, cards):
        self.cards += cards
        pass

    def _possible_game(self):
        for gametype in GameType:
            splits = gametype.name.split('_')
            if splits[0] == "SOLO":
                if self._count_ober() + self._count_unter() == 8:
                    return gametype
                elif self._count_trump(gametype) == 7 and self._count_ober() >= 1 and splits[1].lower() == self._get_color_with_most_cards():
                    return gametype
                elif self._count_trump(gametype) == 6\
                        and self._count_colors_without_trump_color_without_A(gametype) < 2 \
                        and self._count_ober() > 1 \
                        and not (self._count_ober() == 2 and self._count_unter() < 2)\
                        and splits[1].lower() == self._get_color_with_most_cards():
                    return gametype
                elif self._count_trump(gametype) == 5 and self._count_ober() == 4 and self._count_colors_without_trump_color_without_A(gametype) == 1\
                        and splits[1].lower() == self._get_color_with_most_cards_without_A():
                    return gametype
            elif splits[0] == "SAUSPIEL" and self._count_trump(gametype) > 3 and self._count_colors_without_trump_color_without_A(gametype) < 3:
                color = splits[1].lower()
                if sum(1 if c == color else 0 for f, c in self.cards if f not in ["U", "O"]) > 0 and not ("A", color) in self.cards:
                    #print("choosing sauspiel with {}".format(self.cards))
                    return gametype
                else:
                    #print("wanting to ({}) but cant {}".format(color, self.cards))
                    continue
        return False

    def _get_trump_cards(self, gametype):
        inds = []
        for i,card in enumerate(self.cards):
            if gametype.is_trump(card):
                inds.append(i)
        return inds

    def _count_ober(self):
        return sum(1 if f=='O' else 0 for f, c in self.cards)

    def _count_unter(self):
        return sum(1 if f=='U' else 0 for f, c in self.cards)

    def _cards_without_O_U(self):
        return [c for c in self.cards if c[0] not in ["O", "U"]]

    def _count_colors_without_trump_color(self, gametype):
        return len(Counter([c for f, c in self.cards if not gametype.is_trump((f, c))]))

    # Number of "Fehlfarben"
    def _count_colors_without_trump_color_without_A(self, gametype):
        return len(Counter([c for f, c in self.cards if not gametype.is_trump((f, c))
                    if not c == "A" ]))

    def _count_cards_color_most_cards(self):
        trump_faces = ["O", "U"]
        cards_without_trumpface = [c for c in self.cards if
                                   c[0] not in trump_faces]
        versch_farb = Counter(
            [col for f, col in cards_without_trumpface])
        return versch_farb[max(versch_farb, key=versch_farb.get)]

    def _get_color_with_most_cards(self):
        trump_faces = ["O", "U"]
        cards_without_trumpface = [c for c in self.cards if
                                   c[0] not in trump_faces]
        versch_farb = Counter(
            [col for f, col in cards_without_trumpface])
        return max(versch_farb, key=versch_farb.get)

    def _get_color_with_most_cards_without_A(self):
        trump_faces = ["O", "U"]
        cards_without_trumpface = [c for c in self.cards if
                                   c[0] not in trump_faces if c[0] != "A"]
        versch_farb = Counter(
            [col for f, col in cards_without_trumpface])
        return max(versch_farb, key=versch_farb.get)

    def _count_trump(self, gametype):
        return sum(1 if gametype.is_trump(card) else 0 for card in self.cards)

    def wuerde(self, array_of_wuerdes, min_game):
        pos = self._possible_game()
        if pos and pos >=  min_game:
            return True
        return False

    def want_to_play(self, array_of_games_wuerdes_and_weiters, min_game):
        pos = self._possible_game()
        if pos >=  min_game:
            return pos
        #print("game not high enough")
        return False

    def info_gameplayer(self, gameplayer, gametype):
        self.gameplayer = gameplayer
        self.gametype = gametype

    def kontra_chance(self, card):
        raise Exception("not implemented")
    def retour_chance(self, card):
        raise Exception("not implemented")

    def _get_trump_cards(self, gametype):
        inds = []
        for i,card in enumerate(self.cards):
            if gametype.is_trump(card):
                inds.append(i)
        return inds

    def play_on_turn(self,turn):
        try:
            if len(self.cards) == 1:
                turn.place_card(self.cards, 0)
                return
            #TODO depend on gametype
            gametype, game_color = self.gametype.split_in_name_and_color()
            trumpinds = self._get_trump_cards(turn.gametype)
            if len(turn.stack) == 0:
                if gametype == 'SAUSPIEL':
                    if ("A", game_color) in self.cards \
                    and sum(1 if c == game_color else 0 for f, c in self.cards) < 4 \
                    and len(trumpinds) > 0:
                        turn.place_card(self.cards, trumpinds[0])
                        return
                    elif ("A", game_color) in self.cards:
                        turn.place_card(self.cards, self.cards.index(("A", game_color)))
                        return
                turn.place_card(self.cards, 0)
                return
            base = turn.stack[0]
            higheststackcard = base
            if gametype == 'SAUSPIEL' and not turn.gametype.is_trump(base) and base[1] == game_color and ("A", game_color) in self.cards:
                turn.place_card(self.cards, self.cards.index(("A", game_color)))
                return
            for card in turn.stack[1:]:
                if turn.gametype.compare_cards(higheststackcard, card):
                    higheststackcard = card
            if turn.gametype.is_trump(base):
                # search for higher trump card - if none found, play lowest
                # improve on decision to take the stich or not
                if len(trumpinds) == 0:
                    if turn.gametype == GameType.RAMSCH:
                        highercard = 0
                        for i,card in enumerate(self.cards):
                            if turn.gametype.compare_cards(self.cards[highercard], card):
                                highercard = i
                        turn.place_card(self.cards, highercard)
                        return

                    lowestcard = 0
                    if gametype == 'SAUSPIEL' and self.cards[lowestcard] == ("A", game_color):
                        lowestcard = (lowestcard + 1) % len(self.cards)
                    for i,card in enumerate(self.cards):
                        if turn.gametype.compare_cards(card, self.cards[lowestcard]):
                            if gametype != 'SAUSPIEL' or self.cards[i] != ("A", game_color):
                                lowestcard = i
                    turn.place_card(self.cards, lowestcard)
                    #print("lowest card, no trumps")
                    return

                if turn.gametype == GameType.RAMSCH:
                    lowestcard = trumpinds[0]
                    for ind in trumpinds:
                        card = self.cards[ind]
                        if turn.gametype.compare_cards(card, self.cards[lowestcard]):
                            lowestcard = ind
                            break
                    turn.place_card(self.cards, lowestcard)
                    return

                highercard_ind = None
                for ind in trumpinds:
                    card = self.cards[ind]
                    if turn.gametype.compare_cards(higheststackcard, card):
                        highercard_ind = ind
                        break
                if highercard_ind is not None:
                    turn.place_card(self.cards, highercard_ind)
                    #print("higher trump")
                    return
                else:
                    lowestcard = trumpinds[0]
                    for i in trumpinds:
                        if turn.gametype.compare_cards(self.cards[i], self.cards[lowestcard]):
                            lowestcard = i
                    turn.place_card(self.cards, lowestcard)
                    #print("lowest trump")
                    return
            else:
                # try to place higher same color or no points
                # if not same color then play trump if possible, else no points
                # TODO improve on decisions
                base = turn.stack[0]
                higheststackcard = base
                for card in turn.stack[1:]:
                    if turn.gametype.compare_cards(higheststackcard, card):
                        higheststackcard = card

                indswithcolor = []
                for i,card in enumerate(self.cards):
                    face, color = card
                    if color == base[1] and not turn.gametype.is_trump(card):
                        indswithcolor.append(i)

                if len(indswithcolor) == 0:
                    # we are free to play what we want
                    if len(trumpinds) == 0 or turn.gametype == GameType.RAMSCH:
                        lowestcard = 0
                        if gametype == 'SAUSPIEL' and self.cards[lowestcard] == ("A", game_color):
                            lowestcard = (lowestcard + 1) % len(self.cards)
                        for i,card in enumerate(self.cards):
                            if turn.gametype.compare_cards(card, self.cards[lowestcard]):
                                if gametype != 'SAUSPIEL' or self.cards[i] != ("A", game_color):
                                    lowestcard = i
                        turn.place_card(self.cards, lowestcard)
                        #print("lowest card")
                        return
                    else:
                        # for now just choose a random trump
                        turn.place_card(self.cards, trumpinds[0])
                        #print("random trump")
                        return
                else:
                    if turn.gametype.is_trump(higheststackcard):
                        if turn.gametype == GameType.RAMSCH:
                            highercard = indswithcolor[0]
                            for i in indswithcolor:
                                if turn.gametype.compare_cards(self.cards[highercard], self.cards[i]):
                                    highercard = i
                            turn.place_card(self.cards, highercard)
                            #print("lowest card with color, someone had trump")
                            return

                        lowestcard = indswithcolor[0]
                        for i in indswithcolor:
                            if turn.gametype.compare_cards(self.cards[i], self.cards[lowestcard]):
                                lowestcard = i
                        turn.place_card(self.cards, lowestcard)
                        #print("lowest card with color, someone had trump")
                        return
                    else:
                        lowestcard = indswithcolor[0]
                        highercard = None
                        for i in indswithcolor:
                            if turn.gametype.compare_cards(higheststackcard, self.cards[i]):
                                highercard = i
                            if turn.gametype.compare_cards(self.cards[i], self.cards[lowestcard]) and turn.gametype != GameType.RAMSCH:
                                lowestcard = i
                        if highercard is not None:
                            turn.place_card(self.cards, highercard)
                            #print("higher card with color")
                            return
                        turn.place_card(self.cards, lowestcard)
                        #print("lowest card with color")
                        return
        except CannotPlayCardError as e:
            raise Exception("Smart Player played invalid!")
        raise Exception("seems I have missed something")

    def info_stichresult(self,stich):
        pass
    def rundenende(self,results,info):
        pass
