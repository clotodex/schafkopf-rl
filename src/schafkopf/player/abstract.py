from .. import util
from ..util import CannotPlayCardError
from ..gametype import GameType
from collections import Counter

class AbstractPlayer(object):
    def reset(self):
        raise Exception("not overridden error")
    def __init__(self, name):
        raise Exception("not overridden error")
    def info_table(self, first_ind, your_ind, names): #maybe even player info like name, points etc
        raise Exception("not overridden error")
    def deal_first4(self, cards):
        # can return a knock
        raise Exception("not overridden error")
    def info_knocked(self, player):
        raise Exception("not overridden error")
    def deal_second4(self, cards):
        raise Exception("not overridden error")
    # eine würde runde
    def wuerde(self, array_of_wuerdes):
        raise Exception("not overridden error")
    # n offene runde oder weiter bis das höchste spiel gefunden wurde
    def want_to_play(self, array_of_games_wuerdes_and_weiters, min_game):
        raise Exception("not overridden error")
    def info_gameplayer(self, gameplayer, gametype):
        raise Exception("not overridden error")

    # TODO bis inklusiv die erste karte liegt kann man kontra
    # dann kann man retour -> dann geht das spiel zurück an die spieler
    # kann sich NICHT wiederholen (a la sauspiel.de)
    # (auch genannt stoß)
    def kontra_chance(self, card):
        # TODO async, da nur solange zweite karte nicht gelegt ist
        raise Exception("not overridden error")
    def retour_chance(self, card):
        # TODO async, da nur solange zweite karte nicht gelegt ist
        raise Exception("not overridden error")
    def info_kontra(self, player):
        raise Exception("not overridden error")
    def info_retour(self, player):
        raise Exception("not overridden error")

    def play_on_turn(self,turn):
        raise Exception("not overridden error")
    def info_stichresult(self,stich):
        raise Exception("not overridden error")
    def rundenende(self,results,info):
        raise Exception("not overridden error")

    #TODO abrechnung
