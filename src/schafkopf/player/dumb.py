from .. import util
from ..util import CannotPlayCardError
from ..gametype import GameType
from collections import Counter
from .abstract import AbstractPlayer

class DumbPlayer(AbstractPlayer):
    def reset(self):
        self.cards=[]
        self.first=None
        self.ind=None
        self.gameplayer=None
        self.gametype=None
        self.playernames=None

    def __init__(self, name):
        self.name = name
        self.reset()

    def info_table(self, first_ind, your_ind, names): #maybe even player info like name, points etc
        self.first = first_ind
        self.ind = your_ind
        self.playernames = names

    def deal_first4(self, cards):
        self.cards += cards
        return False

    def info_knocked(self, player):
        pass

    def deal_second4(self, cards):
        self.cards += cards
        pass

    def wuerde(self, array_of_wuerdes, min_game):
        return False

    def want_to_play(self, array_of_games_wuerdes_and_weiters, min_game):
        return False

    def info_gameplayer(self, gameplayer, gametype):
        self.gameplayer = gameplayer
        self.gametype = gametype

    def kontra_chance(self, card):
        raise Exception("not implemented")
    def retour_chance(self, card):
        raise Exception("not implemented")

    def play_on_turn(self,turn):
        ind = 0
        while True:
            try:
                turn.place_card(self.cards, ind)
                break
            except CannotPlayCardError as e:
                ind += 1
                #if ind >= len(self.cards):
                    #print("ERROR")

    def info_stichresult(self,stich):
        pass
    def rundenende(self,results,info):
        pass
