__all__ = [
        'abstract',
        'dumb',
        'smart',
        ]
from .abstract  import AbstractPlayer
from .dumb  import DumbPlayer
from .smart  import SmartPlayer
