from enum import Enum, auto, unique
from . import util
from .util import CannotPlayCardError
from functools import cmp_to_key

@unique
class GameType(Enum):
    # TODO do all other gamemodes
    # TODO what about Hochzeit and similar?

    # solo
    # TODO might all be the same value => new gamevalue function
    SOLO_HERZ = auto()
    SOLO_EICHEL = auto()
    SOLO_GRAS = auto()
    SOLO_SCHELLEN = auto()

    # wenz
    WENZ = auto()
    WENZ_HERZ = auto()
    WENZ_EICHEL = auto()
    WENZ_GRAS = auto()
    WENZ_SCHELLEN = auto()

    # gaier
    GEIER = auto()
    GEIER_HERZ = auto()
    GEIER_EICHEL = auto()
    GEIER_GRAS = auto()
    GEIER_SCHELLEN = auto()

    # sau
    SAUSPIEL_EICHEL = auto()
    SAUSPIEL_GRAS = auto()
    SAUSPIEL_SCHELLEN = auto()

    # ramsch
    RAMSCH = auto()

    # TODO this could mess with enum internals
    def __lt__(self, other):
        return self.value > other.value
    def __le__(self, other):
        return self.value >= other.value
    def __gt__(self, other):
        return self.value < other.value
    def __ge__(self, other):
        return self.value <= other.value

    def is_sauspiel(self):
        return self.split_in_name_and_color()[0] == "SAUSPIEL"

    def split_in_name_and_color(self):
        splits = self.name.split('_')
        if len(splits) == 1:
            return splits[0], None
        elif len(splits) == 2:
            return splits[0], splits[1].lower()
        else:
            raise ValueError("Unknown gametype format: {}, {}".format(self, splits))

    def get_trumps_ordered(self):
        cards = [(f,c) for f in util.faces for c in util.colors if self.is_trump((f,c))]
        cards.sort(key=cmp_to_key(self.compare_cards))
        return cards[::-1]

    def split_in_faces_and_color(self):
        return self._split_in_faces_and_color()

    def _split_in_faces_and_color(self):
        splits = self.name.split('_')
        overtype = splits[0]
        color = None
        if len(splits) == 2:
            color = splits[1].lower()
        if overtype == "SAUSPIEL":
            color = "herz"
        if overtype in ["SOLO", "RAMSCH", "SAUSPIEL"]:
            return ["O", "U"], color
        elif overtype == "WENZ":
            return ["U"], color
        elif overtype == "GEIER":
            return ["O"], color
        else:
            raise ValueError("Unkown gametype")

    def is_trump(self, card):
        faces, color = self._split_in_faces_and_color()
        f,c = card
        return color == c or f in faces

    def compare_cards(self, highest, test):
        # highest is of the correct color (or trump)
        trumpfaces, trumpcolor = self._split_in_faces_and_color()
        if self.is_trump(highest):
            if self.is_trump(test):
                highest_face, highest_color = highest
                test_face, test_color = test
                if highest_face == test_face:
                    return util.colors.index(test_color) > util.colors.index(highest_color)
                if highest_face in trumpfaces and not test_face in trumpfaces:
                    return False
                if highest_face not in trumpfaces and test_face in trumpfaces:
                    return True
                if highest_face in trumpfaces and test_face in trumpfaces:
                    return util.faces[test_face] > util.faces[highest_face]
                # This is correct since U, and O were tested before with trumpfaces
                return util.faces[test_face] > util.faces[highest_face]
            else:
                return False
        else:
            if self.is_trump(test):
                return True
            highest_face, highest_color = highest
            test_face, test_color = test
            if highest_color != test_color:
                return False
            # FIXME this is wrong
            return util.cardorder.index(test_face) > util.cardorder.index(highest_face)

    # TODO maybe place in Turn to automatically also check for highest card
    # TODO do we need the whole round history here?
    def check_rules(self, cardindex, cards, turn):
        if cardindex >= len(cards):
            raise CannotPlayCardError("you dont have that many cards anymore")
        overtype, color = self.split_in_name_and_color()
        card = cards[cardindex]
        if overtype in ["SOLO","RAMSCH","SAUSPIEL","WENZ", "GEIER"]:
            if len(turn.stack) == 0:
                # if you want to start stich with called color - have to lay A - except if he can run away
                if overtype == "SAUSPIEL" and not self.is_trump(card)\
                        and card != ("A", color) and card[1] == color \
                        and ("A", color) in cards \
                        and sum(1 if c == color else 0 for _, c in cards) < 4 \
                        and len(cards) > 1:
                        raise CannotPlayCardError("you are not allowed to play the ruffarbe because you can not run away")
                    #TODO mark when ran away and make all other rules obsolete
                return
            base = turn.stack[0]
            if overtype == "SAUSPIEL":
                if base[1] == color.lower() and not self.is_trump(base):
                # have to place A when searching
                    if ("A", color.lower()) in cards and card != ("A", color.lower()):
                        raise CannotPlayCardError("you have to play the rufsau here {}, turn: {}".format(cards,turn.stack))
                elif card == ("A", color.lower()) and len(cards) > 1:
                # rufsau darf nicht geschmiert werden
                    raise CannotPlayCardError("you are not allowed to play the rufsau '{}', '{}'".format(card, cards))
            if self.is_trump(base):
                if self.is_trump(card):
                    return
                if not any(self.is_trump(c) for c in cards):
                    return
                raise CannotPlayCardError("you have to play a trump card")
            else:
                _, basecolor = base
                _, color = card
                if color == basecolor and not self.is_trump(card):
                    return
                if not any(c[1] == basecolor and not self.is_trump(c) for c in cards):
                    return
                raise CannotPlayCardError("you have to give color '{}' but played '{}'".format(basecolor, card))
        else:
            raise ValueError("Unimplemented")
        if False:
            raise CannotPlayCardError("reason")
