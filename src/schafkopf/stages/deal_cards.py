from .stage import Stage
from .. import util
import random
class DealCardsStage(Stage):
    def prepare_cards(self):
        self.cards = [(face, color) for face in util.faces for color in util.colors]
        random.shuffle(self.cards)

    def prepare_players(self):
        self.context.first = (self.context.dealer_ind + 1) % 4
        self.rotation = self.context.first
        self.context.players = self.context.players[self.rotation:]+self.context.players[:self.rotation]
        self.context.rotation = self.rotation
        #print("Players: ", [p.name for p in self.context.players])
        for i,p in enumerate(self.context.players):
            p.info_table(0, i, [p.name for p in self.context.players])

    def __init__(self, context):
        """
        initialize variables
        """
        # deal one player at a time and handle knocking
        self.context = context
        self.prepare_cards()
        self.prepare_players()
        self.current_player = 0
        self.context.knocked = [False, False, False, False]

    def advance_player(self):
        self.current_player = (self.current_player + 1) % 4

    def do_step(self):
        """
        do one step of the stage, as small as possible
        """
        if len(self.cards) > 4*4:
            # dealing first 4 cards
            if self.context.players[self.current_player].deal_first4(self.cards[0:4]):
                self.context.knocked[self.current_player] = True
                for pl in self.context.players:
                    pl.info_knocked(p.ind)
        else:
            self.context.players[self.current_player].deal_second4(self.cards[0:4])
        self.cards = self.cards[4::]
        self.advance_player()

        if len(self.cards) == 0:
            import functools
            #for p in self.context.players:
            #    print("'{}': {}".format(p.name, sorted(p.cards, key=util.cardindex)[::-1]))
            return True
