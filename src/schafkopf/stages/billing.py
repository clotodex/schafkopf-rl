from .stage import Stage
from ..gametype import GameType

class BillingStage(Stage):
    def __init__(self, context):
        """
        initialize variables
        """
        self.context = context

    def get_additional_gameplayer(self):
        splits = self.context.gametype.name.split('_')
        if splits[0] == GameType.SAUSPIEL_EICHEL.name.split('_')[0]:
            rufsau = ("A", splits[1].lower())
            for stich in self.context.stiche:
                if rufsau in stich.cards:
                    ind = stich.cards.index(rufsau)
                    player = (ind + stich.kommtraus) % 4
                    return player
        return None
    def do_step(self):
        """
        do one step of the stage, as small as possible
        """

        if self.context.gametype == GameType.RAMSCH:
            results = [ sum(stich.points() for stich in self.context.stiche if stich.winner == i) for i in range(4)]
            loser = results.index(max(results))
            # TODO ramsch
            results = [0,0,0,0]
            info = {}
        else:

            # TODO do this right
            a = 10
            rufspiel = 2 * a
            single = 5 * a

            base = rufspiel if self.context.gametype.is_sauspiel() else single
            schneider = a
            schwarz = a
            laufende_value = a #+ n * a if n >= 3
            # ramsch?

            #TODO if SIE
            #TODO if tout
            #TODO RAMSCH

            laufende = self.context.laufende * laufende_value

            gameplayer_points = sum(stich.points() for stich in self.context.stiche if stich.winner in self.context.gameplayers)
            non_gameplayer_points = 120 - gameplayer_points

            schneider = schneider if gameplayer_points < 30 or non_gameplayer_points < 30 else 0
            stichcount = sum(1 if stich.winner in self.context.gameplayers else 0 for stich in self.context.stiche)
            schwarz = schwarz if stichcount == 8 or stichcount == 0 else 0
            gameprice = base + schneider + schwarz + laufende

            # TODO implement those
            self.context.kontra = False
            self.context.retour = False

            for k in self.context.knocked:
                if k:
                    gameprice *= 2
            if self.context.kontra:
                gameprice *= 2
            if self.context.retour:
                gameprice *= 2

            if self.context.kontra and not self.context.retour:
                win = gameplayer_points >= 60
            else:
                win = gameplayer_points > 60
            win = 1 if win else -1

            # one loser pays one winner in sauspiel
            # nonplayers play player or the other way round
            pay_n = 1 if self.context.gametype.is_sauspiel() else 3
            results = [
                    win * pay_n * gameprice
                    if i in self.context.gameplayers else
                    (-win) * gameprice
                    for i,p in enumerate(self.context.players)]

            info = {
                "schneider": schneider,
                "schwarz": schwarz,
                "laufende": laufende
                # here can be more info
            }

        # TODO FIXME either dont rotate back or dont start rotated to be consistent
        # results = results[-self.context.rotation:]+results[:-self.context.rotation]
        self.context.results = results
        self.context.result_info = info
        for p in self.context.players:
            p.rundenende(results, info)
        return True
