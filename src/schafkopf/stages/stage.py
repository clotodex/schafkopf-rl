class Stage:
    def __init__(self):
        """
        initialize variables
        """
        pass

    def do_step(self):
        """
        do one step of the stage, as small as possible, then return True when done
        """
        raise Exception("Not overridden error")
