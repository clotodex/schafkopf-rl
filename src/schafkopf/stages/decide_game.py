from .stage import Stage
from ..gametype import GameType
class DecideGametypeStage(Stage):
    def __init__(self, context):
        """
        initialize variables
        """
        self.context = context
        self.context.gameplayer = None
        self.context.gametype = None
        self.current_player = 0
        self.wuerde_arr = []

    def advance_player(self):
        self.current_player = (self.current_player + 1) % 4

    def do_step(self):
        """
        do one step of the stage, as small as possible, if done return True
        """
        if len(self.wuerde_arr) < 4:
            self.min_game = [gametype for gametype in GameType][-(sum(self.wuerde_arr)+1+1)]
            w = self.context.players[self.current_player].wuerde(self.wuerde_arr, self.min_game)
            self.wuerde_arr.append(w)
            self.advance_player()
            return

        # dont do 'any' check if it has already been done
        if not hasattr(self, 'game_arr') and not any(self.wuerde_arr):
            # lets play ramsch
            # or kill the game
            self.context.gametype = GameType.RAMSCH
        else:
            if not hasattr(self, 'game_arr'):
                self.game_arr = self.wuerde_arr
                self.min_game = [gametype for gametype in GameType][-(sum(self.game_arr)+1)]

            if self.game_arr.count(False) < 3 or self.game_arr.count(True) > 0:
                if self.game_arr[self.current_player]:
                    g = self.context.players[self.current_player].want_to_play(self.game_arr, self.min_game)
                    if g and g >= self.min_game:
                        self.min_game = GameType(max(1, g.value - 1))
                        self.game_arr = [g == True for g in self.game_arr]
                    else:
                        g = False
                    self.game_arr[self.current_player] = g
                self.advance_player()
            else:
                self.context.gameplayer = [True if g else False for g in self.game_arr].index(True)
                self.context.gametype = self.game_arr[self.context.gameplayer]

        if self.context.gametype is not None:
            for p in self.context.players:
                p.info_gameplayer(self.context.gameplayer, self.context.gametype)

            self.context.gameplayers = [self.context.gameplayer]
            if self.context.gametype.is_sauspiel():
                _, gamecolor = self.context.gametype.split_in_name_and_color()
                self.context.gameplayers += [i for i,p in enumerate(self.context.players) if ("A", gamecolor) in p.cards]

            if self.context.gametype == GameType.RAMSCH:
                self.context.laufende = 0
            else:
                laufende = [0,0]
                for card in self.context.gametype.get_trumps_ordered():
                    if any(card in self.context.players[i].cards for i in self.context.gameplayers):
                        if laufende[1] > 0:
                            break
                        laufende[0] += 1
                    else:
                        if laufende[0] > 0:
                            break
                        laufende[1] += 1
                limit = 2 if self.context.gametype in [GameType.WENZ, GameType.GEIER] else 3
                laufende = min(8, max(laufende))
                if laufende < limit:
                    laufende = 0
                self.context.laufende = laufende

            return True
