from .stage import Stage
from ..gameround import Turn, Stich
class PlayRoundStage(Stage):
    def __init__(self, context):
        """
        initialize variables
        """
        self.context = context
        self.kommtraus = 0
        self.turn = None
        self.current_player = 0
        self.context.stiche = []

    def advance_player(self):
        self.current_player = (self.current_player + 1) % 4

    def do_step(self):
        """
        do one step of the stage, as small as possible
        """
        if self.turn is None:
            self.turn = Turn(self.context.gametype, self.kommtraus)

        self.context.players[self.current_player].play_on_turn(self.turn)
        self.advance_player()

        if len(self.turn.stack) == 4:
            ind = self.turn.determine_winner()
            winner = (ind+self.kommtraus) % 4
            stich = Stich(self.turn.stack, winner, self.kommtraus)
            self.context.stiche.append(stich)
            self.kommtraus = winner
            self.current_player = self.kommtraus
            self.turn = None
            for p in self.context.players:
                p.info_stichresult(stich)
            #print("Stich (started by '{}') was made '{}' by '{}'".format(
            #self.context.players[stich.kommtraus].name, stich.cards, self.context.players[winner].name))

        if len(self.context.stiche) == 8:
            return True
