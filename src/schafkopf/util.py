class CannotPlayCardError(Exception):
    pass

colors = ["schellen", "herz", "gras", "eichel"]
colors_color_cards = ["schellen", "gras", "eichel", "herz"]
faces = {"7": 0, "8": 0, "9": 0, "K": 4, "10": 10, "A": 11, "U": 2, "O": 3}
cardorder = ["7", "8", "9", "U", "O", "K", "10", "A"]

sortedcards = [(f,c) for c in colors_color_cards for f in cardorder if f not in ["O", "U"]] \
            + [(f,c) for f in ["U", "O"] for c in colors]

def cardindex(card):
    return sortedcards.index(card)
