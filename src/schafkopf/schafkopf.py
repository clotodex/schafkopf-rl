from .gameround import Turn, Stich
from .gametype import GameType
from . import util
from enum import Enum, auto
from . import stages
import random

class GameState(Enum):
    DEAL_CARDS          = 1
    DECIDE_ON_GAMETYPE  = 2
    PLAY_ROUND          = 3
    BILLING             = 4
    DONE                = 5

    def next(self):
        if self == GameState.DONE:
            return self
        return GameState(self.value+1)

    def build_stage(self, context):
        if self == GameState.DONE:
            return None
        return self.stages[self](context)

GameState.stages = {
    GameState.DEAL_CARDS          : stages.DealCardsStage,
    GameState.DECIDE_ON_GAMETYPE  : stages.DecideGametypeStage,
    GameState.PLAY_ROUND          : stages.PlayRoundStage,
    GameState.BILLING             : stages.BillingStage
}


class SchafkopfContext:
    pass

class Schafkopf:
    def __init__(self, players, dealer_ind):
        for p in players:
            p.reset()
        self.cardsprinted = False
        self.context = SchafkopfContext()
        self.context.players = players
        self.context.dealer_ind = dealer_ind
        self.state = GameState.DEAL_CARDS
        self.stage = self.state.build_stage(self.context)
        #print("random in schafkopf: ", random.randint(1,10))

    def do_gamestep(self):
        if self.stage.do_step():
            self.state = self.state.next()
            self.stage = self.state.build_stage(self.context)
        #if self.state == GameState.DECIDE_ON_GAMETYPE and not self.cardsprinted:
        #    self.cardsprinted = True
        #    print("players: ",[p.name for p in self.context.players])
        #    print("cards: ")
        #    for p in self.context.players:
        #        print("    -> ",p.cards)
        #if self.state == GameState.DONE:
        #    print("stiche: ", [stich.cards for stich in self.context.stiche])

    def play_stepwise(self):
        while self.state != GameState.DONE:
            self.do_gamestep()
        return (self.context.gametype, self.context.results)
