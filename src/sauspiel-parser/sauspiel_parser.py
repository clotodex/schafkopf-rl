import json

from bs4 import BeautifulSoup
from selenium import webdriver

# initiate
driver = webdriver.Firefox() # initiate a driver, in this case Firefox
driver.get("https://www.sauspiel.de") # go to the url

# locate the login form
username_field = driver.find_element_by_id("ontop_login") # get the username field
password_field = driver.find_element_by_id("login_inline_password") # get the password field

# log in
with open("secrets.json") as secrets_json:
    json_data = json.load(secrets_json)
    username_field.send_keys(json_data['username']) # enter in your username
    password_field.send_keys(json_data['password']) # enter in your password
password_field.submit() # submit it

from time import sleep
sleep(1)

driver.get("https://www.sauspiel.de/spiele?page=1?utf8=%E2%9C%93&user%5B"
           "login%5D=&role=all&game%5Bbalance_type%5D=-1&game%5B"
           "short_deck%5D=-1&game%5Bgame_type_id%5D=-1&game%5B"
           "announcement%5D=-1&game%5Bcontras%5D=-1&game%5B"
           "runners_from%5D=-14&game%5Brunners_to%5D=14&game%5B"
           "won%5D=-1&game%5Bresult%5D=-1")
soup = BeautifulSoup(driver.page_source, 'html.parser')

for element in soup.find_all(class_="card games-item"):
    game_url = element.find(class_="card-header games-item-header").a.get('href')
    print(game_url)


def parse_game(game_url):
    driver.get("https://www.sauspiel.de/spiele/" + game_url)
    soup = BeautifulSoup(driver.page_source, 'html.parser')

    # Spielausgang, Klopfer, Kontra und Retour
    result_table = soup.find(class_="game-result-table")
    for tr in result_table.tbody.find_all('tr'):
        if tr.th.string == 'Spielausgang':
            print('Spielausgang:')
            print(' '.join(tr.td.string.split()))
        if tr.th.string == 'Wert':
            print('Wert:')
            print(' '.join(tr.td.string.split()))
        if tr.th.string == 'Klopfer':
            print('Klopfer:')
            print([player.string for player in tr.td.find_all('a')])
        if tr.th.string == 'Kontra und Retour':
            print('Kontra und Retour:')
            print([player.string for player in tr.td.find_all('a')])
    print('\n\n')

    # Players, Role, Cards first and second
    for element in soup.find_all(class_="card-row game-protocol-item"):
        print(str(element.find(class_="avatar-username").string).strip())
        print(str(element.find(class_="game-participant-role").string).strip())
        first = []
        second = []
        for card in element.find_all(class_="card-image"):
            if 'highlight' in card['class']:
                first.append(card['class'][3])
            else:
                second.append(card['class'][3])
        print(first)
        print(second)
        print()
    print('\n\n')

    # Who would like to play?
    section = soup.find_all(class_="card")[1]
    for element in section.find_all(class_="card-row"):
        print(' '.join(element.string.split()))
    print('\n\n')

    # Stiche
    for i, section in enumerate(soup.find_all(class_="card")[2:10], start=1):
        print("Stich " + str(i))
        print("Stich von " + section.find(class_="avatar")['data-username'])
        for element in section.find_all(class_="game-protocol-trick-card"):
            print(element.a['data-username'] + ': ' + element.span['class'][3])
        print()

    driver.close()


parse_game('904347967-sauspiel-auf-die-hundsgfickte-von-kechtel')
