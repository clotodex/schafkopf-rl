import gym

from stable_baselines.common.policies import MlpPolicy, register_policy
from stable_baselines.common.vec_env import SubprocVecEnv
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import PPO2

from schafkopf.player import SmartPlayer, DumbPlayer
from ai_player import AIPlayer
from hyperparams import load_hyperparams, to_uid

from tensorboard_logging import Logger as TensorboardLogger

import multiprocessing
import time
import os
import random
from tqdm import tqdm
import json

def create_env(codes, ckpt_path, cur_ckpt):
    def make_env(env_id, playerclasses, args):
        """
        Utility function for multiprocessed env.

        :param env_id: (str) the environment ID
        :param num_env: (int) the number of environments you wish to have in subprocesses
        :param seed: (int) the inital seed for RNG
        :param rank: (int) index of the subprocess
        """
        def _init():
            gym.envs.register(
                id=env_id,
                entry_point='schafkopf_gym:SchafkopfEnv',
                kwargs={
                    'playerclasses': playerclasses,
                    'args': args,
                },
            )
            env = gym.make(env_id)
            return env
        #set_global_seeds(seed)
        return _init

    def raiser(ex): raise ex


    env = SubprocVecEnv([
        make_env(
            "CustomSchafkopf-{}-v0".format(code),
            playerclasses = [
                    SmartPlayer if c is 's' else
                    DumbPlayer  if c is 'd' else
                    AIPlayer    for c in code
            ],
            args = [
                    ("Smart P{}".format(i+2),) if c is 's' else
                    ("Dumb P{}".format(i+2),)  if c is 'd' else
                    ("Agent P{}".format(i+2),
                        ckpt_path.format(
                            cur_ckpt - 1    if c is 'I' else
                            cur_ckpt - 5    if c is 'V' else
                            cur_ckpt - 10   if c is 'X' else
                            cur_ckpt - 50   if c is 'L' else
                            cur_ckpt - 100  if c is 'C' else
                            cur_ckpt - 100  if c is 'D' else
                            cur_ckpt - 1000 if c is 'M' else
                            raiser(Exception("c '{}' unimplemented".format(c))
                        )
                    )) for i,c in enumerate(code)
            ]
        ) for code in codes
    ])
    #env = DummyVecEnv([lambda: gym.make(name) for name in names])
    return env

def ordered(obj):
    if isinstance(obj, dict):
        return sorted((k, ordered(v)) for k, v in obj.items())
    if isinstance(obj, list):
        return sorted(ordered(x) for x in obj)
    else:
        return obj

if __name__ == "__main__":
    hyperparams = load_hyperparams()
    uid = to_uid(hyperparams)
    print("UID: ",uid)

    num_runs=500 # 100

    tensorboard_log_dir="./models/train/tensorboard_{}".format(uid)
    checkpoint_path="./models/checkpoints/model_{}".format(uid)
    os.makedirs(checkpoint_path, exist_ok=True)
    checkpointfilename_prefix="schafkopf_chkpt_"
    checkpointfilename=checkpointfilename_prefix + "{}"

    metadatafile = os.path.join(checkpoint_path, "metadata.json")
    if os.path.isfile(metadatafile):
        with open(metadatafile) as f:
            loaded_hyperparams = json.load(f)
            if json.dumps(loaded_hyperparams, sort_keys=True) != json.dumps(json.loads(json.dumps(hyperparams)), sort_keys=True):
                print("ERROR: Hyperparams do not match!")
                print("loaded hyperparams: ", json.dumps(loaded_hyperparams, sort_keys=True))
                print("current hyperparams: ", json.dumps(hyperparams, sort_keys=True))
                print("ABORTING!")
                exit(1)
    else:
        with open(metadatafile, 'w') as outfile:
            json.dump(hyperparams, outfile, sort_keys=True)

    all_agents = [
        int(filename[len(checkpointfilename_prefix):].split('.')[0])
        for filename in os.listdir(checkpoint_path) if os.path.isfile(os.path.join(checkpoint_path, filename)) and filename.startswith(checkpointfilename_prefix)
    ]

    latest_trained_agent = max(all_agents) if all_agents else 0
    print("latest_trained_agent: ",latest_trained_agent)

    cur_run_strategy = hyperparams['run_strategy'][0]
    for key in hyperparams['run_strategy']:
        if latest_trained_agent > key: # TODO maybe >=
            cur_run_strategy = hyperparams['run_strategy'][key]
        else:
            break

    env = create_env(cur_run_strategy, os.path.join(checkpoint_path, checkpointfilename), latest_trained_agent+1)

    # https://stable-baselines.readthedocs.io/en/master/guide/custom_policy.html
    policy_kwargs = dict(net_arch=hyperparams['net_arch'])

    if latest_trained_agent > 0:
        checkpointfile = os.path.join(checkpoint_path, checkpointfilename.format(latest_trained_agent))
        model = PPO2.load(checkpointfile, env=env, verbose=1, tensorboard_log=tensorboard_log_dir)
    else:
        model = PPO2(MlpPolicy, env, policy_kwargs=policy_kwargs, nminibatches=len(cur_run_strategy), verbose=1, tensorboard_log=tensorboard_log_dir)

    for i in range(latest_trained_agent+1,num_runs+1):
        #TODO last agent needs to be updated every run!
        if i in hyperparams['run_strategy'] or any(c in ['IVXLCDM'] for code in cur_run_strategy for c in code):
            _start = time.time()
            print("recreating envs")
            cur_run_strategy = hyperparams['run_strategy'][i]
            env.close()
            del env
            env = create_env(cur_run_strategy, os.path.join(checkpoint_path, checkpointfilename), i)
            model.set_env(env)
            if len(cur_run_strategy) != model.nminibatches:
                print("WARNING need to resetup model!")
                model.nminibatches = len(cur_run_strategy)
                model.setup_model()
            _end = time.time()
            print("env update took {}".format(_end-_start))

        # Pass reset_num_timesteps=False to continue the training curve in tensorboard
        # By default, it will create a new curve
        print()
        stratstr = "## Strategy: {} ##".format(cur_run_strategy)
        print("#" * len(stratstr))
        runstr = "RUN {}".format(i)
        _l = len(stratstr) - 6 - len(runstr)
        lpad = _l // 2
        rpad = _l - lpad
        print("##{}{}{}##".format(' '*lpad, runstr, ' '*rpad))
        print(stratstr)
        print("#" * len(stratstr))
        print()

        timesteps = hyperparams['numepisodes'] * 8
        model.learn(total_timesteps=timesteps, reset_num_timesteps=False, tb_log_name="run#{}".format(i))
        model.save(os.path.join(checkpoint_path, checkpointfilename.format(i)))
