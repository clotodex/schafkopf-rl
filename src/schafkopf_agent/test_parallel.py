import argparse
from os import listdir
from os.path import isfile, isdir, join, exists, basename
import time

from multiprocessing import Pool
import multiprocessing as mp

from tqdm import tqdm
import random
import gym
from stable_baselines.common.policies import MlpPolicy
from stable_baselines.common.vec_env import SubprocVecEnv
from stable_baselines import PPO2
from schafkopf.player import SmartPlayer, DumbPlayer
from schafkopf.gametype import GameType
from tensorboard_logging import Logger as TensorboardLogger
import numpy as np

from schafkopf_gym.environment import SchafkopfEnv

from hyperparams import load_hyperparams, to_uid

class SmartPlayerModel():
    def __init__():
        self.player = SmartPlayer("Smart")

def _create_env(args):
    classes, names = args
    return SchafkopfEnv(
        playerclasses=classes,
        args=names
    )

class EnvWrapper(object):
    def __init__(self,test_seed, codes, pool):
        self.test_seed = test_seed
        self.codes = codes
        self.envs = {}
        self.resetted_envs = {}
        self.resetted_obs = []
        self.resetted_infos = []
        self.pool = pool

    def create(self,num_test_games):
        import copy
        print("creating resetted envs...")
        self.num_test_games = num_test_games
        for code in self.codes:
            self.resetted_envs[code] = [None] * self.num_test_games
            # TODO using map is quicker but will not give progress
            _start = time.time()
            playerclasses=[SmartPlayer if c is 's' else DumbPlayer for c in code]
            args=[(("Smart P{}" if c is 's' else "Dumb P{}").format(i+2),) for i,c in enumerate(code)]
            e = SchafkopfEnv(playerclasses=playerclasses, args=args)
            e.seed(self.test_seed)
            self.resetted_envs[code][-1] = e
            for i in range(self.num_test_games):
                newe = copy.deepcopy(self.resetted_envs[code][i-1])
                ob, info = newe.reset()
                self.resetted_envs[code][i] = newe
                self.resetted_obs.append(ob)
                self.resetted_infos.append(info)
            _end = time.time()
            print("took {}s".format(_end-_start))
        print("created")

    def reset_async(self):
        pass
    #    self._r = pool.map(_forward_reset, [
    #        (code,i,env, self.forwards[i], self.test_seed)
    #        for code, envs in self.envs.items()
    #        for i,env in enumerate(envs)
    #    ])#, chunksize=100)
    #    #for i,env,res in [_forward_reset((i,env, self.forwards[i], self.test_seed)) for i,env in enumerate(envs)]:#, chunksize=100)

    def reset_await(self):
        return self.reset()
        #obs = []
        #infos = []
        #for code,i,env,(ob, info) in self._r:
        #    self.envs[code][i] = env
        #    obs.append(ob)
        #    infos.append(info)
        #return obs, infos


    def reset(self):
        import copy
        self.envs = copy.deepcopy(self.resetted_envs)
        return self.resetted_obs, self.resetted_infos
        #print("resetting envs...")
        #obs = []
        #infos = []
        #for code,envs in self.envs.items():
        #    _start = time.time()
        #    it = pool.map(_forward_reset, [(code,i,env, self.forwards[i], self.test_seed) for i,env in enumerate(envs)])#, chunksize=100)
        #    results = [None] * len(envs)
        #    #for i,env,res in [_forward_reset((i,env, self.forwards[i], self.test_seed)) for i,env in enumerate(envs)]:#, chunksize=100)
        #    for code,i,env,res in it:
        #        results[i] = res
        #        self.envs[code][i] = env
        #    for ob, info in results:
        #        obs.append(ob)
        #        infos.append(info)
        #    _end = time.time()
        #    print("took {}s".format(_end-_start))
        #for code, envs in self.envs.items():
        #    for env in envs:
        #        if not hasattr(env, 'agent'):
        #            print("reset afterwards faulty")
        ## TODO vstack all observations and infos
        #print("resetted")
        #return obs, infos


    def step(self, actions):
        # TODO WARNING randomstate is copied from parent => if step needs random, you will need to save and set the randomstate
        # This is currently not needed since resetting is the only randomized thing
        # FIXME parallelizing step was very slow
        obs = []
        rewards = np.zeros(shape=(len(self.codes), self.num_test_games))
        dones = []
        infos = []
        for i,code in enumerate(self.envs):
            envs = self.envs[code]
            for j,env in enumerate(envs):
                ob, reward, done, info = env.step(actions[i*len(envs)+j])
                obs.append(ob)
                rewards[i,j] = reward
                dones.append(done)
                infos.append(info)
        return obs, rewards, dones, infos

    def get_attr(self, attr):
        return {code: [getattr(env, attr) for env in envs] for code, envs in self.envs.items()}

def run_test(run, checkpointfile, test_env, test_seed, num_test_games, test_writers, codes):

    # prepare envs and model
    start = time.time()
    test_env.reset_async()
    model = PPO2.load(checkpointfile)

    __start = time.time()
    obs, infos = test_env.reset_await()
    __end = time.time()
    print("reset took: {}".format(__end - __start))

    step_rewards = []
    dones = [False]
    # game loop
    while not any(dones): # TODO should be replaced by step numer = num_games*8 and use the auto reset
        valid_actions = np.vstack([np.array(info['valid_actions']) for info in infos])
        actions, _states = model.predict(obs, deterministic=True, action_mask=valid_actions)
        obs, rewards, dones, infos = test_env.step(actions)
        step_rewards.append(rewards)

    # shape (4,8,1000)
    all_rewards = np.array(step_rewards).swapaxes(0,1)
    games = test_env.get_attr('game') # 0.87s for 1000 calls

    # avg reward
    avg_reward = {}
    for i,code in enumerate(test_env.codes):
        avg_reward[code] = np.sum(all_rewards[i,:,:]) / test_env.num_test_games

    # TODO mean avg_reward and variance avg_reward!

    # TODO create baseline using smart player (and all other player combinations

    # win percentage
    own_index = test_env.get_attr('agent_inds')
    gameresults = {code: {gt.split_in_name_and_color()[0]: [0,0] for gt in GameType} for code in test_env.codes}
    own_gameresults = {code: {gt.split_in_name_and_color()[0]: [0,0] for gt in GameType} for code in test_env.codes}
    gametypes = {code: [game.context.gametype.split_in_name_and_color()[0] for game in gs] for code,gs in games.items()}
    for i,code in enumerate(test_env.codes):
        for j,gt in enumerate(gametypes[code]):
            if all_rewards[i,7,j] > 0:
                gameresults[code][gt][0] += 1
                if games[code][j].context.gameplayer == own_index[code][j][0]:
                    own_gameresults[code][gt][0] += 1
            else:
                gameresults[code][gt][1] += 1
                if games[code][j].context.gameplayer == own_index[code][j][0]:
                    own_gameresults[code][gt][1] += 1

    ## summary generation

    ## Adding values to tensorboard (https://gist.github.com/gyglim/1f8dfb1b5c82627ae3efcfbbadb9f514)
    #avg_reward = acc_reward / num_test_games

    for i,code in enumerate(codes):
        for gt,(won,lost) in gameresults[code].items():
            if (won+lost) == 0:
#               print("GameType {} never played!".format(gt))
                continue
            # TODO dont format for code (done already with directories)
            test_writers[code].log_scalar("{}/{}-win-percentage".format(code,gt), (won/(won+lost))*100, run)
            print("{}/{}-win-percentage - {} ({} games) - {}".format(code,gt, won/(won+lost), (won+lost), run))

        for gt,(won,lost) in own_gameresults[code].items():
            if (won+lost) == 0:
#               print("GameType {} never played!".format(gt))
                continue
            test_writers[code].log_scalar("{}/angesagt-{}-win-percentage".format(code,gt), (won/(won+lost))*100, run)
            print("{}/angesagt-{}-win-percentage - {} ({} games) - {}".format(code,gt, won/(won+lost), (won+lost), run))

        test_writers[code].log_scalar("{}/avg reward".format(code), avg_reward[code], run)
        print("{}/avg reward - {} - {}".format(code, avg_reward[code], run))

        test_writers[code].writer.flush()

    end = time.time()
    print("finished test ({})".format(end-start))

    # win percentage per gametype
    # per gegen or spieler or mitspieler

    # parallelize execution and data collection but write tensorboard statistics in one thread (3 models loaded)
    # or execute different envs in parallel and collect them together to be processed by one model

    #TODO more detailed
    # - wins per gametype
    # - best practices (selbst suchen etc)
    #TODO test on all player combinations to make all trainings comparable

    #TODO parallelize to training
    # TODO have 3 parallel testers one for every player combination

if __name__ == "__main__":

    hyperparams = load_hyperparams()
    uid = to_uid(hyperparams)
    print("UID: ",uid)

    checkpoint_dir=""
    tensorboard_dir=""

    parser = argparse.ArgumentParser(description='Testing model checkpoints.')

    parser.add_argument('-i','--interval', type=float, default=10.000, help="the time interval before the next check for new checkpoints (in seconds, float possible)")
    parser.add_argument('-l','--last-checkpoint', type=int, help="if this checkpoint is encountered, then terminate")
    parser.add_argument('-t','--terminate', action="store_true", help="process all existing checkpoints and immediately terminate, overrides interval")
    #TODO utilize
    parser.add_argument('-j','--cpus', type=int, default=1, help="the number of cpu cores to use")

    parser.add_argument('-p','--prefix', help="text before the checkpoint number in the file name")
    parser.add_argument('-s','--suffix', help="text after the checkpoint number in the file name")

    #TODO format as directory (trailing / and all)
    parser.add_argument('-c','--checkpoint-dir', default="./models/checkpoints/model_{}".format(uid), help="the directory containing the checkpoint files")
    parser.add_argument('-b','--tensorboard-dir', default="./models/test/test_{}".format(uid), help="the directory to store tensorboard logs to")

    parser.add_argument('--seed', type=int, default=7357, help="the seed used for testing")
    parser.add_argument('-g','--games', type=int, default=1000, help="number of test games to play")

    args = parser.parse_args()

    tested_file = ".tested"

    pool = Pool(mp.cpu_count())  # (n) number of threads equal to number of CPUs

    codes = ['ddd', 'dds', 'dss', 'sss']
    test_env = EnvWrapper(args.seed, codes, pool)
    test_env.create(args.games)

    checkpointfiles = {}
    if exists(join(args.tensorboard_dir, tested_file)):
        print("found .tested file")
        with open(join(args.tensorboard_dir, tested_file), "r") as tested:
            lines = [line.rstrip('\n') for line in tested]
            lines = [line[len(args.checkpoint_dir):] for line in lines if line.startswith(args.checkpoint_dir)]
            print("found {} already completed testruns".format(len(lines)))
            checkpointfiles = {"done-{}".format(i): basename(line) for i,line in enumerate(lines)}

    test_writers = {}
    for code in codes:
        test_writers[code] = TensorboardLogger(join(args.tensorboard_dir,code))

    terminate = False
    while not terminate:
        terminate = args.terminate

        # find new checkpoints
        new_checkpointfiles = []
        if isdir(args.checkpoint_dir):
            new_checkpointfiles = [
                f for f in listdir(args.checkpoint_dir)
                if isfile(join(args.checkpoint_dir, f))
                if f.startswith(args.prefix)
                if f.endswith(args.suffix)
                if f not in checkpointfiles.values()
            ]

        if new_checkpointfiles:
            new_checkpointfiles = {int(filename[len(args.prefix):len(filename)-len(args.suffix)]): filename for filename in new_checkpointfiles}

            # test all checkpoint files
            for i, filename in sorted(new_checkpointfiles.items()):
                print("testing checkpoint #{}".format(i))
                run_test(i, join(args.checkpoint_dir,filename), test_env, args.seed, args.games, test_writers, codes)

                with open(join(args.tensorboard_dir, tested_file), "a") as tested:
                    tested.write("{}\n".format(join(args.checkpoint_dir,filename)))

                if i == args.last_checkpoint:
                    print("Encountered last checkpoint {} - terminating".format(i))
                    exit()

            checkpointfiles.update(new_checkpointfiles)
        else:
            print("no new checkpoints")

        if args.terminate:
            print("Processed all checkpoint files - terminating")
            exit()

        print("waiting for new checkpoints...")
        # sleep interval
        time.sleep(args.interval)
