import gym
from stable_baselines import PPO2
from stable_baselines.common.vec_env import DummyVecEnv
from schafkopf.player import SmartPlayer, DumbPlayer

if __name__ == "__main__":

    gym.envs.register(
        id='CustomSchafkopf-v0',
        entry_point='schafkopf_gym:SchafkopfEnv',
        kwargs={
            'playerclasses': [SmartPlayer, SmartPlayer, DumbPlayer],
            'args': [("Smart P2",), ("Smart P3",), ("Dumb P4",)],
        },
    )
    env = DummyVecEnv([lambda: gym.make('CustomSchafkopf-v0') for i in range(1)])

    i = 100
    model = PPO2.load("checkpoints/ppo2_schafkopf_{}".format(i))

    # Enjoy trained agent
    obs, info = env.reset()
    env.render()
    done = False
    while not done:
        input("press ENTER key to continue")
        print("info: ",info)
        action, _states = model.predict(obs, action_mask=info['valid_actions'])
        obs, reward, done, info = env.step(action)
        reward, done, info = reward[0], done[0], info[0]
        env.render()
        print("reward: {}{}".format(reward, " - DONE" if done else ""))
