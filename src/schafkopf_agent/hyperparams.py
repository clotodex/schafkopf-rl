import os
import json

def load_hyperparams():
    default_net_arch = [dict(pi=[64,64], vf=[64,64])]
    shared_net_arch = [64, 64, 64]
    seperate_net_arch = [dict(pi=[128, 128, 128], vf=[128, 128, 128])]
    mixed_net_arch = [128, dict(pi=[128, 128], vf=[128, 128])] # can also exclude vf or pi

    hyperparams_file = "hyperparams.json"
    if not os.path.isfile(hyperparams_file):
        print("ERROR no '{}' found - ABORTING".format(hyperparams_file))
        exit(1)

    # TODO maybe use protobuf (pbtxt) instead of json
    hyperparams = None
    with open(hyperparams_file) as f:
        hyperparams = json.load(f)
    if hyperparams is None:
        print("ERROR while loading hyperparams - ABORTING")
        exit(1)

    # would be prevented by protobuf
    run_strategy = hyperparams['run_strategy']
    hyperparams['run_strategy'] = {int(k):v for k,v in run_strategy.items()}

    return hyperparams

    # d = dumb
    # s = smart
    # I = 1 run ago (latest)
    # V = 5 runs ago
    # X = 10 runs ago
    # L = 50 runs ago
    # C = 100 runs ago
    # D = 500 runs ago
    # M = 1000 runs ago

    #metadata = json.dumps(hyperparams, sort_keys=True)
    #uid = hash(metadata)

def format_netarch(netarch):
    if type(netarch[-1]) is not dict:
        return "{}_0_0".format('-'.join(netarch))
    shared = netarch[:-1]
    seperate = netarch[-1]
    return "{}_{}_{}".format(
            '0' if len(shared) == 0 else '-'.join((str(s) for s in shared)),
            '-'.join((str(s) for s in seperate['pi'])) if 'pi' in seperate else '0',
            '-'.join((str(s) for s in seperate['vf'])) if 'vf' in seperate else '0')

def to_uid(hyperparams):
    uid = "v0.0.0_{}_ep{}_{}_{}".format(hyperparams['model'], hyperparams['numepisodes'], "runstrategy", format_netarch(hyperparams['net_arch']))
    return uid
