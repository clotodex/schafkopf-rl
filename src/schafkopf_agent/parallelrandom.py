from multiprocessing import Pool
import multiprocessing as mp
import random

from schafkopf_gym import SchafkopfEnv
from schafkopf.player import DumbPlayer

def _forward_reset(args):
    i,seed = args
    random.seed(seed)
    for _ in range(i):
        random.random()
    return i, random.randrange(100)

seed = 3457

def perform(pool, num):
    it = pool.map(_forward_reset, [(i, seed) for i in range(num)])#, chunksize=100)
    res = [0] * num
    for i,r in it:
        res[i] = r
    return res

def is_sublist(l, s):
    sub_set = False
    if s == []:
        sub_set = True
    elif s == l:
        sub_set = True
    elif len(s) > len(l):
        sub_set = False
    else:
        for i in range(len(l)):
            if l[i] == s[0]:
                n = 1
                while (n < len(s) and (i+n) < len(l)) and (l[i+n] == s[n]):
                    n += 1
                if n == len(s):
                    sub_set = True
    return sub_set

def findmatch(l1,l2):
    for i in range(len(l1)+1):
        sub = l1[0:i]
        if not is_sublist(l2, sub):
            break
    if i > 0:
        return (i, 0)
    for j in range(len(l2)+1):
        sub = l2[0:j]
        if not is_sublist(l1, sub):
            break
    if j > 0:
        return (0, j)

#pool = Pool(mp.cpu_count())  # (n) number of threads equal to number of CPUs
#for i in range(5):
#    res = perform(pool, 100)
#    print(res)

def find_match(l1, l2):
    first = (-1,-1)
    for i,x in enumerate(l1):
        for j,y in enumerate(l2):
            if x != y:
                break
        if j-1 > first[1]:
            first = (i, j-1)
    second = (-1,-1)
    for i,x in enumerate(l2):
        for j,y in enumerate(l1):
            if x != y:
                break
        if j-1 > second[1]:
            second = (i, j-1)
    return first,second

def findmatch2(l1, l2):
    first = (-1, -1)
    for i,x in enumerate(l1):
        if x == l2[0]:
            sublist = []
            for j,y in enumerate(l2):
                if i+j >= len(l1):
                    break
                x = l1[i+j]
                if x != y:
                    break
            if j == 0:
                print("ERROR")
            if j > first[1]:
                first = (i,j)
    second = (-1, -1)
    for i,x in enumerate(l2):
        if x == l1[0]:
            sublist = []
            for j,y in enumerate(l1):
                if i+j >= len(l2):
                    break
                x = l2[i+j]
                if x != y:
                    break
            if j == 0:
                print("ERROR")
            if j > second[1]:
                second = (i,j)
    return first, second





################
### DRY TEST ###
################

def randomize(a,n,b):
    random.shuffle(a)
    z = random.randrange(n)
    random.shuffle(b)
    return z

a = [i for i in range(4)]
b = [i for i in range(32)]
a = [0] * 4
b = [0] * 32

random.seed(seed)
[randomize(a,4,b) for _ in range(5)]
x = [random.randrange(10) for i in range(30)]

random.seed(seed)
[randomize(a,4,b) for _ in range(5)]
y = [random.randrange(10) for i in range(30)]

(i1,j1),(i2,j2) = findmatch2(x,y)
if j1 == j2:
    print("PERFECT!")
elif j1 > j2:
    print("x started matching at ",i1)
    print("y got matched until",j1)
    print(x[:j1],x[j1:])
    print(y[:i1],y[i1:])
else:
    print("x started matching at ",i2)
    print("y got matched until",j2)
    print(x[:j2],x[j2:])
    print(y[:i2],y[i2:])

################
### WET TEST ###
################

# create env
env = SchafkopfEnv(playerclasses = [DumbPlayer, DumbPlayer, DumbPlayer])

# reset n times
env.seed(seed)
env.reset()
print("resets:", env._resets)

# get forwards
forwards = (env._resets - 1 +1)# * (numrands)
print("forwards: ", forwards)
env.reset()
print("resets 2:", env._resets)
forwards = forwards + (env._resets)
print("forwards 2: ", forwards)
#print("total 2: ", forwards/(numrands))
correct_random = [random.randrange(100) for i in range(30)]

random.seed(seed)
print("sum: ",sum([randomize(a,4,b) for i in range(forwards)]))
fake_random = [random.randrange(100) for i in range(30)]

(i1,j1),(i2,j2) = findmatch2(correct_random,fake_random)
if j1 < 0 and j2 < 0:
    print("full dismatch!")
elif j1 == j2:
    print(correct_random)
    print(fake_random)
    print("PERFECT!",i1,j1,i2,j2)
elif j1 > j2:
    print("{} too many fake randoms used".format(i1))
    print(correct_random[:i1],correct_random[i1:])
    print(fake_random[:j1],fake_random[j1:])
else:
    print("{} fake randoms unused".format(i2))
    print(correct_random[:i2],correct_random[i2:])
    print(fake_random[:j2],fake_random[j2:])

# notes
