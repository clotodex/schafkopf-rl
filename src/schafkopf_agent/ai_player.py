from schafkopf_gym.observation_player import ObservationPlayer
from schafkopf_gym.observation import onehot_observation, observation_space
from stable_baselines import PPO2
from schafkopf.util import CannotPlayCardError

import os
import contextlib


class AIPlayer(ObservationPlayer):
    def __init__(self, name, checkpointfile):
        # dont print warnings
        with open(os.devnull, "w") as f, contextlib.redirect_stdout(f):
            self.model = PPO2.load(checkpointfile)
        super().__init__(name)

    def play_on_turn(self, turn):
        # find valid cards
        valid_actions = self.get_valid_actions(turn)
        # get observation
        obs = onehot_observation(self, turn)
        # predict with model
        actions, _states = self.model.predict([obs], deterministic=True, action_mask=[valid_actions])
        turn.place_card(self.cards, actions[0])

    def get_valid_actions(self, turn):
        def check_card(i):
            try:
                self.gametype.check_rules(i, self.cards, turn)
                return True
            except CannotPlayCardError as e:
                return False

        return [check_card(i) for i in range(8)]
