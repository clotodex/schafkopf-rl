import gym
from stable_baselines import PPO2
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines.common import set_global_seeds

from schafkopf.player import SmartPlayer, DumbPlayer

from tqdm import tqdm
import random
import numpy as np

if __name__ == "__main__":

    random.seed(7357)
    set_global_seeds(7357)
    np.random.seed(7357)

    gym.envs.register(
        id='CustomSchafkopf-v0',
        entry_point='schafkopf_gym:SchafkopfEnv',
        kwargs={
            'playerclasses': [SmartPlayer, SmartPlayer, DumbPlayer],
            'args': [("Smart P2",), ("Smart P3",), ("Dumb P4",)],
        },
    )

    def make_env(env_id, rank, seed=0):
        """
        Utility function for multiprocessed env.

        :param env_id: (str) the environment ID
        :param num_env: (int) the number of environments you wish to have in subprocesses
        :param seed: (int) the inital seed for RNG
        :param rank: (int) index of the subprocess
        """
        def _init():
            env = gym.make(env_id)
            env.seed(seed + rank)
            return env
        set_global_seeds(seed)
        return _init

    env_id = "CartPole-v1"
    num_cpu = 4  # Number of processes to use
    # Create the vectorized environment
    #env = SubprocVecEnv([make_env(env_id, i) for i in range(num_cpu)])
    env = DummyVecEnv([make_env('CustomSchafkopf-v0',i,7357) for i in range(1)])
    #env = DummyVecEnv([lambda: gym.make('CustomSchafkopf-v0') for i in range(1)])

    for e in env.envs:
        e.seed(7357)
    i = 100

    # Enjoy trained agent
    #random.seed(7357)
    obs, info = env.reset()
    total_reward = []
    for i in tqdm(range(1,5)):
        print("new run")
        random.seed(7357)
        set_global_seeds(7357)
        np.random.seed(7357)
        model = PPO2.load("training/checkpoints_rrr/ppo2_schafkopf_{}".format(i))
        random.seed(7357)
        set_global_seeds(7357)
        np.random.seed(7357)
        for e in env.envs:
            e.seed(7357)
        acc_reward = 0
        for i in tqdm(range(1,3)):
        #    print("game #{} (acc reward: {})".format(i,acc_reward))
            done = False
            while not done:
                action, _states = model.predict(obs, action_mask=info['valid_actions'])
                obs, reward, done, info = env.step(action)
                reward, done, info = reward[0], done[0], info[0]
                acc_reward += reward
        print("acc reward: ",acc_reward)
        total_reward.append(acc_reward)
    print("total reward: ",total_reward)
