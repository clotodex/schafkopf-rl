import gym
from stable_baselines import PPO2
from stable_baselines.common.vec_env import DummyVecEnv, SubprocVecEnv
from stable_baselines.common import set_global_seeds

from schafkopf.player import SmartPlayer, DumbPlayer

from tqdm import tqdm
import random
import numpy as np
import time

SEED=7357

def create_env(test_seed):
    def make_env(env_id, rank, seed=0):
        """
        Utility function for multiprocessed env.

        :param env_id: (str) the environment ID
        :param num_env: (int) the number of environments you wish to have in subprocesses
        :param seed: (int) the inital seed for RNG
        :param rank: (int) index of the subprocess
        """
        def _init():
            env = gym.make(env_id)
            env.seed(seed)# + rank)
            return env
        #set_global_seeds(seed)
        return _init

    def register(code):
        playerclasses = [SmartPlayer if c is 's' else DumbPlayer for c in code]
        args = [(("Smart P{}" if c is 's' else "Dumb P{}"),).format(i+2) for i,c in enumerate(code)]
        print("registering 'TestSchafkopf-{}-v0'".format(code))
        gym.envs.register(
            id='TestSchafkopf-{}-v0'.format(code),
            entry_point='schafkopf_gym:SchafkopfEnv',
            kwargs={
                'playerclasses': playerclasses,
                'args': args,
            },
        )
    codes = ['ddd'] #, 'dds', 'dss', 'sss']
    for code in codes:
        register(code)
    #test_env = SubprocVecEnv([make_env("TestSchafkopf-{}-v0".format(code), test_seed) for code in codes])
    test_env = DummyVecEnv([make_env("TestSchafkopf-{}-v0".format(code), test_seed) for code in codes])
    return test_env

if __name__ == "__main__":

    # Create the vectorized environment
    #env = SubprocVecEnv([make_env(env_id, i) for i in range(num_cpu)])
    #env = SubprocVecEnv([make_env('CustomSchafkopf-v0',i,SEED) for i in range(4)])
    env = create_env(SEED)
    #env = DummyVecEnv([lambda: gym.make('CustomSchafkopf-v0') for i in range(1)])

    #for e in env.envs:
    #    e.seed(SEED)
    i = 100
    model = PPO2.load("training/checkpoints_rrr/ppo2_schafkopf_{}".format(i))

    # Enjoy trained agent
    #random.seed(SEED)
    obs, infos = env.reset()
    #print(env.game.context.cards) # TODO either supply info with debug flag or dont use ready made vec envs
    total_reward = []
    for i in tqdm(range(1,6)):
        print("new run")
        #random.seed(SEED)
        #env.seed(SEED)
        start = time.time()
        env.env_method('seed', SEED)

        #np.random.seed(SEED)
        #set_global_seeds(SEED)
        obs, infos = env.reset()
        #for e in env.envs:
        #    e.seed(SEED)
        acc_reward = [0,0,0,0]
        for i in tqdm(range(1,1001)):
        #    print("game #{} (acc reward: {})".format(i,acc_reward))
            dones = [False]
            while not any(dones): # TODO should be replaced by step numer = num_games*8 and use the auto reset
                valid_actions = np.vstack([np.array(info['valid_actions']) for info in infos])
                actions, _states = model.predict(obs, deterministic=True, action_mask=valid_actions)
                obs, rewards, dones, infos = env.step(actions)
                for i,reward in enumerate(rewards):
                    acc_reward[i] += reward
        end = time.time()
        print("TIME TAKEN: ",end-start)
        print("acc reward: ",acc_reward)
        total_reward.append(acc_reward)
    print("total reward: ",total_reward)

    def forward(n):
        return [random.randrange(10) for i in range(n)]

    start = time.time()
    env.env_method('seed', SEED)
    for i in range(1,1001):
        env.reset()
    end = time.time()
    print("TIME TAKEN: ",end-start)

    #TODO does not account for ramsch
    #random.seed(SEED)
    #print("new after seed: ",random.randrange(10))
    #newrandoms = []
    #for i in range(1,2):
    #    print("forward: ",forward(3))
    #    print("new after shuffle: ",random.randrange(10))
    #    print("forward: ",forward(1+31))
    #    print("new after cards: ",random.randrange(10))
    #    newrandoms.append(random.randrange(10))
    #print("newrandoms: ",newrandoms)
