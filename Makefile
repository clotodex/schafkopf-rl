.NOTPARALLEL:

.PHONY : main external all train clean workflow

main:
	python setup.py install
	pytest tests

external:
	cd external/stable-baselines && \
	python setup.py install

all: | external main

train:
	python src/schafkopf_agent/betteragent.py

test:
	python src/schafkopf_agent/test_parallel.py -c "checkpoints/" -b "ppo2_schafkopf_tensorboard/test/" -t -p "ppo2_schafkopf_" -s ".pkl"

clean:
	pip uninstall -y schafkopf
	pip uninstall -y stable-baselines

workflow: | clean all train
