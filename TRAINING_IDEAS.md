# Training Ideas

## Training setup idea:

- 100 runs with ddd
- 100 runs with dds
- 100 runs with dss
- 100 runs with sss
- 100 runs with agents

## Possible opponents:

- ddd,
- dds
- dss,
- sss,
- best agent out of last 100
- best agent out of last 50
- best agent out of last 10
- last agent

### maybe even:

- ddd-trained-agent
- dds-trained-agent
- dss-trained-agent
- sss-trained-agent

- with the SAME agent in environment (4 agents, all step and update same policy net)

### which opponents on same table

- when to train on what table
- -> better exploration with multiple different opponents
- -> or better exploration in game with same opponents
- maybe gradual switch?
- supervised dataset

## observations

- [DONE] normalize indices
- [DONE] remove knocking etc for now (no data)
- [DONE] keep information of where you sit

- [DONE] better gametypes
- [DONE] solo vs team
- [DONE] subgame for solo
- [DONE] or ruffarbe
- [DONE] trumpf color
- [DONE] ober, unter information

## reward rework

- dont penalize bad cards

## test metrics

- Elo
- Trueskill
- win percentage of gameplayer games
- test for skills, how often does he search himself

## connect to game decider network

## important hyper parameters

- neural network size
- run size (epochs, batchsize, etc)
- parallel environments

how to perform tests?
maybe include metrics in training on training data to better judge models

hyperparameter search
per paramcombination 5 runs? what to do with randomization?

no reward on stich?

## other ideas:

- train on seed
- mixed network with supervised prediction of who is mitspieler
- sub networks after observation (mini fully connected on cards etc
- random network distillation for intrinsic motivation
- count based reward
- harder tests: predict who the agent played with (supervised classification head)
- other observations
- normalization of batches and layers
- use cnn or even pooling
