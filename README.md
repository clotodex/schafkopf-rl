# Schafkopf

Idea: build a reinforcement agent to learn how to play schafkopf

Steps:
- [x] carddeck
- [x] turns
- [x] build the gamerules (trumping and similar)
- [ ] transform to stepable gym environment
- [ ] learn agent

## Testing

setup environmen

    virtualenv --no-site-packages venv
    source venv/bin/activate
	pip3 install pytest

testing

    python setup.py install
	pytest
