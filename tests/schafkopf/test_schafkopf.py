from schafkopf.player import DumbPlayer, SmartPlayer
from schafkopf.schafkopf import Schafkopf
from schafkopf.gametype import GameType
import random

def test_main():
    playerconfigs = [[
        DumbPlayer("Dumb A"),
        DumbPlayer("Dumb B"),
        DumbPlayer("Dumb C"),
        DumbPlayer("Dumb D"),
    ],[
        SmartPlayer("Smart A"),
        DumbPlayer("Dumb B"),
        DumbPlayer("Dumb C"),
        DumbPlayer("Dumb D"),
    ],[
        SmartPlayer("Smart A"),
        SmartPlayer("Smart B"),
        DumbPlayer("Dumb C"),
        DumbPlayer("Dumb D"),
    ],[
        SmartPlayer("Smart A"),
        SmartPlayer("Smart B"),
        SmartPlayer("Smart C"),
        DumbPlayer("Dumb D"),
    ],[
        SmartPlayer("Smart A"),
        SmartPlayer("Smart B"),
        SmartPlayer("Smart C"),
        SmartPlayer("Smart D"),
    ]]
    for players in playerconfigs:
        rounds = []
        gametypes = []
        dealer = random.randrange(4)
        for i in range(1000):
            game = Schafkopf(players, dealer)
            gametype, results = game.play_stepwise()
            #test for same amount of points if in the same team
            assert sum(p for p in results) == 0
            rounds.append(results)
            gametypes.append(gametype)
            dealer = (dealer + 1) % 4
