import gym

def test_gym():
    env = gym.make('schafkopf_gym:Schafkopf-v0')
    obs, info = env.reset()

    print("observation space: ",env.observation_space)
    print("action space: ",env.action_space)

    assert env.observation_space is not None
    assert env.action_space is not None

    total_reward = 0.
    done = False
    while not done:
        env.render()
        ind = -1
        for i,a in enumerate(info['valid_actions']):
            if a:
                ind = i
                break
        obs,reward,done,info = env.step(ind)#env.action_space.sample()) # take a random action
        assert (done or not any(x is None for x in obs))
        # test for out of bounds fail
        print("validactions: ",info['valid_actions'])
        print("cardlen: ",len(env.agents[env.current_idx].cards))
        if len(env.agents[env.current_idx].cards) < 8 and len(env.agents[env.current_idx].cards) > 0:
            assert not all(info['valid_actions'][::-(8-len(env.agents[env.current_idx].cards))])
        total_reward += reward
        print("reward: ",reward)
    env.close()
    print("total reward: ",total_reward)
